﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RezStampCharge : MonoBehaviour {

    Transform player;
    public Color color;
    public AnimationCurve alphaCurve;
    public float maxTime;
    float curTime = 0;
    Material mat;
    bool isActive = false;

	// Use this for initialization
	void Start () {
        mat = this.GetComponent<Projector>().material;
        player = GameObject.Find("Player").transform;
    }
	
	// Update is called once per frame
	void Update () {
        if (isActive)
        {
            curTime += TME_Manager.getDeltaTime(TME_Time.type.player);
            if (curTime >= maxTime)
            {
                onEnd();
            }
            else
            {
                float tn = curTime / maxTime;
                float a = alphaCurve.Evaluate(tn);
                mat.SetColor("_Color", new Color(color.r, color.g, color.b, a));
                mat.SetFloat("_TimeTime", tn);
            }
        }
    }

    public void activate()
    {
        isActive = true;
        curTime = 0;
        transform.position = player.position + Vector3.up * 10;
    }

    void onEnd()
    {
        isActive = false;
    }
}
