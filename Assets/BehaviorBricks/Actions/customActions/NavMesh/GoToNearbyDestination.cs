﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/NavMesh/GoToNearbyDestination")]
    [Help("Moves the monster towards random nearby position w/in 10 units")]
    public class GoToNearbyDestination : GOAction
    {
        public override void OnStart()
        {
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                body.goToNearbyPosition();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}