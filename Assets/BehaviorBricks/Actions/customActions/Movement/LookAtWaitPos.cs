﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/LookAtWaitPos")]
    [Help("rotate to look towards the monster's wait position")]
    public class LookAtWaitPos : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body monster = gameObject.GetComponent<ENT_Body>();
                //Debug.Log(gameObject.name + "got to lookAtPlayer");
                monster.shouldRotate = true;
                monster.lookTowardsVec = monster.WaitPos;
                //monster.ActionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}