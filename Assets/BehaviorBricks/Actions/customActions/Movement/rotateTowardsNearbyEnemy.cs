﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/rotateTowardsNearestEnemy")]
    [Help("Tells the character to rotate towards the nearest enemy")]
    public class rotateTowardsNearestEnemy : GOAction
    {

        //private DBG_MonsterTest monster;
        //private float elapsedTime;

        public override void OnStart()
        {
            ENT_Body body = gameObject.GetComponent<ENT_Body>();
            body.shouldRotate = true;
            body.lookTowardsVec = body.allyManager.getNearestEnemy(gameObject.transform.position).transform.position;
            //body.rotate(true, 5F);
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
