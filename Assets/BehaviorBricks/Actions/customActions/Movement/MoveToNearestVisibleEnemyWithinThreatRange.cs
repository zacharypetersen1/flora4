﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System;

namespace BBUnity.Actions
{

    [Action("Custom/Movement/MoveToNearestVisibleEnemyWithinThreatRange")]
    [Help("Moves the monster towards the nearest enemy position within the allymanager.threatenedRange of twig")]
    public class MoveToNearestVisibleEnemyWithinThreatRange : GOAction
    {
        public override void OnStart()
        {
            //Debug.Log("TemplateAction");

            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestEnemy = am.getNearestVisibleEnemyWithinSphere(gameObject.transform.position, body.sightRange, am.transform.position, am.threatenedRange, false);
                if (nearestEnemy != null)
                {
                    Vector3 vec = gameObject.transform.position - nearestEnemy.transform.position;
                    Vector3 dirFromTarget = vec.normalized;
                    Vector3 finalPos;
                    if (body.entity_type == ENT_DungeonEntity.monsterTypes.treeMinion)
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.25F);
                    }
                    else if (body.entity_type == ENT_DungeonEntity.monsterTypes.mushroomMinion)
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.5F);
                    }
                    else
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.8F);
                    }
                    //sqrmagnitude instead of distance
                    float directSqrDist = UTL_Math.sqrDistance(gameObject.transform.position, nearestEnemy.transform.position);
                    float finalPosSqrDist = UTL_Math.sqrDistance(gameObject.transform.position, nearestEnemy.transform.position + finalPos);
                    if (directSqrDist < finalPosSqrDist)
                    {
                        //Debug.Log("Minion decided it should not move");
                        finalPos = gameObject.transform.position;
                    }
                    else
                    {
                        finalPos = nearestEnemy.transform.position + finalPos;
                    }
                    /*
                    if (body.entity_type == ENT_DungeonEntity.monsterTypes.mushroomMinion)
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.5F);
                    }
                    else if (body.entity_type == ENT_DungeonEntity.monsterTypes.treeMinion)
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.25F);
                    }
                    else
                    {
                        finalPos = dirFromTarget * (body.attackRange * 0.8F);
                    }
                    finalPos = nearestEnemy.transform.position + finalPos;
                    */
                    if (!body.isAtPosition(finalPos))
                        body.goToPosition(finalPos);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}