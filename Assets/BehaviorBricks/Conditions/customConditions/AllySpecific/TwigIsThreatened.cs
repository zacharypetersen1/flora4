﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/twigIsThreatened")]
    [Help("checks if an enemy is within threatenedRange of twig")]
    public class twigIsThreatened : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                PLY_AllyManager am = body.allyManager;
                GameObject nearestEnemy = am.getNearestEnemyToTwig();
                if (nearestEnemy == null)
                {
                    //Debug.Log("nearestEnemy is null in checkTwigIsThreatened");
                    return false;
                }
                bool result = Vector3.Distance(nearestEnemy.transform.position, am.transform.position) < am.threatenedRange;
                am.twigIsThreated = result;
                //Debug.Log("twig is threatened res: " + result);
                return result;
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}