﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/NotNearWaitPos")]
    [Help("Checks if the monster is notwithin IndividualFollowRadius of their waitPos")]
    public class NotNearWaitPos : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                //Debug.Log("self not near twig res: " + !body.amINearTwig(gameObject.transform.position));
                return !body.isAtPosition(body.myGoToPos, PLY_AllyManager.DISTANCE_TO_STRAY_FROM_GOTO); // the extra number is to make this more of an attack move command so that they can fight enemies near the destination
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}