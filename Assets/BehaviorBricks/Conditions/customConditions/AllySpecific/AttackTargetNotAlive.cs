﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/AllySpecific/AttackTargetNotAlive")]
    [Help("true if AttackTarget is null")]
    public class AttackTargetNotAlive : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                return body.AttackTarget == null;

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}