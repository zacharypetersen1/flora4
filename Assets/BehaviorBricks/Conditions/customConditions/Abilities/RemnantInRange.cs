﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/Abilities/RemnantInRange")]
    [Help("check if any remnants are in range of the necro ability")]
    public class RemnantInRange : GOCondition
    {

        public override bool Check()
        {
            ENT_NecroMinion necro = gameObject.GetComponent<ENT_NecroMinion>();
            return necro.remnantsVisible();
        }
    }
}
