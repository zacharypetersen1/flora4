﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;

namespace BBUnity.Conditions
{
    [Condition("Custom/LOS/LOSToPlayer")]
    [Help("checks if the monster can see the player")]
    public class LOSToPlayer : GOCondition
    {
        ENT_Body monsterBody;

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                //ENT_Brain monsterBrain = gameObject.GetComponent<ENT_Brain>();
                monsterBody = gameObject.GetComponent<ENT_Body>();
                return monsterBody.CanSeePlayer();
            }
            catch
            {
                throw new UnityException("error in checkLOS node");
            }
        }
    }
}