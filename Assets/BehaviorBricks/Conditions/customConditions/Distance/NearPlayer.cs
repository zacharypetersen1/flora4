﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;

namespace BBUnity.Conditions
{
    [Condition("Custom/Distance/NearPlayer")]
    [Help("checks if the monster is near the player")]
    public class NearPlayer : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkAllyDistance");
            try
            {
                //ENT_Brain monsterBrain = gameObject.GetComponent<ENT_Brain>();
                ENT_Body monsterBody = gameObject.GetComponent<ENT_Body>();
                return monsterBody.isNearPlayer(10);
            }
            catch
            {
                throw new UnityException("error in checkAllyDistance node");
            }
        }
    }
}