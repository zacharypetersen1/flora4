﻿using UnityEngine;
using Pada1.BBCore.Framework;
using Pada1.BBCore;
using System;

namespace BBUnity.Conditions
{
    [Condition("Custom/Template")]
    [Help("A template for condition leaf nodes")]
    public class TempCondition : GOCondition
    {

        public override bool Check()
        {
            //Debug.Log("checkLOS");
            try
            {
                ENT_Body body = gameObject.GetComponent<ENT_Body>();
                return true; //monsterTest.ConditionFunctionName();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}