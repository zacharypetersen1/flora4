﻿Shader "Flora/Cel1"
{
	Properties
	{
		[NoScaleOffset] _MainTex("Main Texture", 2D) = "white" {}
		[NoScaleOffset] _RampTex("Ramp Texture", 2D) = "gray" {}
		_RampTexScalar("Ramp Texture Scalar", float) = 1
		_RampLevels("Ramp Levels", int) = 2
		_RampSize("Ramp Size", Range(0, 1)) = 0.3
		_HighIntensity("High Light Intensity", float) = 1
		_LowIntensity("Low Light Intensity", float) = 0
		_CutoffIntensity("Intensity Cutoff", float) = 0.5
	}
	
	SubShader
	{
		Pass
		{
			// indicate that our pass is the "base" pass in forward
			// rendering pipeline. It gets ambient and main directional
			// light data set up; light direction in _WorldSpaceLightPos0
			// and color in _LightColor0
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc" // for UnityObjectToWorldNormal
			#include "UnityLightingCommon.cginc" // for _LightColor0

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float3 locPos : TEXCOORD1;
				fixed4 diff : COLOR0; // diffuse lighting color
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata_base v)
			{
				v2f o;
				o.locPos = v.vertex;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord;
				// get vertex normal in world space
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				// dot product between normal and light direction for
				// standard diffuse (Lambert) lighting
				half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				// factor in the light color
				o.diff = nl * _LightColor0;
				return o;
			}

			sampler2D _MainTex;
			sampler2D _RampTex;
			float _RampTexScalar;
			float _RampSize;
			int   _RampLevels;
			float _HighIntensity;
			float _LowIntensity;
			float _CutoffIntensity;

			fixed4 frag(v2f i) : SV_Target
			{
				// sample texture
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 rem_col = col;
				
				// sample ramptex
				float rampIntensity = clamp(i.diff.b / 3, 0, 0.99) + ((tex2D(_RampTex, i.uv).b) * _RampSize) ;
				//fixed4 rampCol = ;
				
				// determine level
				float rampLevel = floor(rampIntensity * (_RampLevels / _CutoffIntensity));
				float multiplier = _LowIntensity + ((_HighIntensity - _LowIntensity) / _RampLevels) * rampLevel;

				// multiply by lighting
				//col *= rampIntensity > _CutoffIntensity ? _HighIntensity : _LowIntensity + 0.4 * i.diff;// +clamp(i.locPos.y, 0, 1)*0.3;
				col *= multiplier;
				col *= (0.85 +  clamp(i.locPos.y, 0, 1)*0.3);
				//col += rem_col * .3 * i.diff;
				/*col += i.diff / 2;
				if (i.diff > _CutoffIntensity)
				{
					col *= .5;
				}*/
				return col;
			}
				
			ENDCG
		}
	}
}