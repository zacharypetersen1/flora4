﻿Shader "Flora/MagicPulse"
{
	Properties
	{
		_Color("Color", Color) = (1, 1, 1, 1)
		_OutlineSize("Outline Size", Range(0, 2)) = 0
		_Alpha("Alpha", Range(0, 1)) = 1
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		Cull front
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
			};

			float4 _Color;
			float _OutlineSize;
			float _Alpha;
			v2f vert (appdata v)
			{
				v2f o;
				float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				worldPos.xyz = worldPos.xyz + worldNormal * _OutlineSize;
				o.vertex = mul(UNITY_MATRIX_VP, worldPos);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = _Color;
				col.a = _Alpha;
				return col;
			}
			ENDCG
		}
	}
}
