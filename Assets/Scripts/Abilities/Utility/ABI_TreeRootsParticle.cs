﻿// Zach

using UnityEngine;
using System.Collections;

public class ABI_TreeRootsParticle : MonoBehaviour {

    ParticleSystem ps;
    ParticleSystem.EmissionModule em;



    //
    // Use this for initialization
    //
    void Start () {
        ps = gameObject.GetComponent<ParticleSystem>();
        em = ps.emission;
    }
	


    //
    // turns particle system off and on
    //
	public void setEnabled(bool value)
    {
        em.enabled = value;
    }
}
