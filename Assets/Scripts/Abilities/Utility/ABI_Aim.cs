﻿// Zach

using UnityEngine;
using System.Collections;

public class ABI_Aim : MonoBehaviour {

    CHA_Motor motor;
    public float speed = 2f;



    //
    // Use this for initialization
    //
    void Start () {
        motor = gameObject.GetComponent<CHA_Motor>();
	}



    //
    // Update is called once per frame
    //
    void Update () {
        if (INP_PlayerInput.getButtonDown("ActFromAim"))
        {
            //UTL_GameObject.cloneAtLocation("Abilities/RoseBush/RoseBushSystem", transform.position, -0.1f);
            GameObject.Find("Player").GetComponent<ABI_Manager>().reduceAbilityCharge(ABI_Manager.abilityType.Cleansing);
            Destroy(gameObject);
        }
	}



    //
    // Runs at fixed rate
    //
    void FixedUpdate()
    {
        transform.Translate(INP_PlayerInput.get2DAxis("AbilityAim", false) * speed, Space.World);
    }
}
