﻿// Zach

using UnityEngine;
using System.Collections;

public class ABI_ParticleSystem : MonoBehaviour {

    public float radius = 5;
    //max number of partciles before destroyed
    public float emissionMax = 30;
    //per frame
    public float emissionRate = 2;
    private float particleCount = 0;
    public float lifeTime = 8f;
    public float particleSize = 1f;
    public float growTime = 0.5f;
    public string ParticlePath;



	//
	// Use this for initialization
	//
	void Start () {
	
	}
	


	//
	// Update is called once per frame
	//
	void Update () {

        // check if it's time to emit last particles and destroy emitter
	    if(particleCount + emissionRate > emissionMax)
        {
            for(int i = 0; i < emissionMax - particleCount; i++)
            {
                emit();
            }
            Destroy(gameObject);
        }

        // or else emit "emissionRate" number of particles
        else
        {
            for(int i = 0; i < emissionRate; i++)
            {
                emit();
            }
            particleCount += emissionRate;
        }
	}



    //
    // Emits a single particle
    //
    void emit()
    {

        // randomly select location within circle
        float r = Random.Range(0, 1f);
        float theta = Random.Range(0, 2*Mathf.PI);
        float y = (Mathf.Sqrt(r) * Mathf.Sin(theta)) * (radius - particleSize / 2);
        float x = (Mathf.Sqrt(r) * Mathf.Cos(theta)) * (radius - particleSize / 2);
        Vector3 location = new Vector3(transform.position.x + x, transform.position.y + y, transform.position.z);

        // check if plant cannot grow at this location
        /*if (!MAP_NatureMap.inBounds(location))
        {
            return;
        } */
        if(MAP_NatureMap.natureTypeAtPos(location) == MAP_NatureData.type.rock)
        {
            return;
        }

        // instantiate particle object
        GameObject temp;
        temp = UTL_Resources.cloneAtLocation(ParticlePath, location);
        /*switch (particleType)
        {
            case ABI_Manager.abilityType.Cleansing:
                temp = UTL_GameObject.cloneAtLocation("Abilities/RoseBush/RoseBushParticle", location); break;
            case ABI_Manager.abilityType.Utility2:
                temp = UTL_GameObject.cloneAtLocation("Abilities/Shrubs/ShrubsParticle", location); break;
            default: throw new UnityException("Unable to find particle that matches type: " + particleType);
        }*/
        ABI_Particle temp_particle = temp.GetComponent<ABI_Particle>();
        temp_particle.lifeTime = lifeTime;
        temp_particle.growTime = growTime;
        temp_particle.size = particleSize;
        temp_particle.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360f)), Space.World);
        temp_particle.transform.localScale = Vector3.zero;
    }
}
