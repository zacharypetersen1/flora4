﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_SlowAbility : MonoBehaviour {

    ENT_SlowMinion body;
    bool purity = false;
    public float slowAbilityRadius = 15;
    public float duration = 3;
    float timer;
    float timePerGrowth;
    int slowFlowerCount;
    public bool Purity
    {
        get { return purity; }
        set { purity = value; }
    }

    public Material ghostMat;

    //float lifeTimer = 22;
    
    // Use this for initialization
    void Start () {
        body = GetComponentInParent<ENT_SlowMinion>();
        slowFlowerCount = Mathf.FloorToInt(slowAbilityRadius * 1.75F);
        body.slowFlowers = new ArrayList(slowFlowerCount);
        //spawnSlowFlowers(slowFlowerCount);
        timePerGrowth = duration / slowFlowerCount;
        timer = duration;

        spawnSlowFlowers(slowFlowerCount);
    }

    // Update is called once per frame
    void Update() {

        if (timer < 0 || body == null || body.gameObject == null)
        {    
            Destroy(gameObject);
        }
        //lifeTimer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        timer -= TME_Manager.getDeltaTime(TME_Time.type.enemies);
        //int flowersToSpawn = Mathf.FloorToInt((duration - timer) / timePerGrowth) - body.slowFlowers.Count;
	}
    

    void spawnSlowFlowers(int flowerCount)
    {
        for (int i = 0; i < flowerCount; ++i)
        {
            spawnSlowFlower(body.isGhost);
        }
    }

    void spawnSlowFlower(bool ghost)
    {
        GameObject slowFlower;
        if (purity)
        {
            slowFlower = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/SlowAbility/slowObj", UTL_Dungeon.getRandomPointInCircle(transform.position, slowAbilityRadius));
        }
        else
        {
            slowFlower = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/SlowAbility/slowObjCor", UTL_Dungeon.getRandomPointInCircle(transform.position, slowAbilityRadius));
        }
        slowFlower.transform.position = UTL_Dungeon.snapPositionToTerrain(slowFlower.transform.position);
        slowFlower.GetComponent<ENV_SlowObj>().purity = purity;
        slowFlower.transform.position -= new Vector3(0, slowFlower.GetComponent<ENV_SlowObj>().distToGrow, 0);
        if (ghost)
        {
            slowFlower.GetComponent<MeshRenderer>().material = ghostMat;
        }
        body.slowFlowers.Add(slowFlower);
    }

}
