﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_CommandFollow : ABI_Ability {

    public override float getRechargeTime()
    {
        return 0.0f;
    }

    //
    // constructor
    //
    public ABI_CommandFollow(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
    }



    //
    // activate Command Follow
    //
    public override void buttonDown()
    { 
        player.GetComponent<PLY_AllyManager>().setAllAlliesState(PLY_AllyManager.allyState.follow);
    }
}
