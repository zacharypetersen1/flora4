﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_CommandActivate : ABI_Ability {


    public override float getRechargeTime()
    {
        return 0.0f;
    }

    //
    // constructor
    //
    public ABI_CommandActivate(PLY_PlayerDriver setDriver, GameObject twig, ABI_Manager setManager)
    {
        base.storeReferences(setDriver, twig, setManager);
    }



    //
    // activate TreeRoots
    //
    public override void buttonDown()
    {
        Debug.Log("activate button down");
        player.GetComponent<PLY_AllyManager>().setAllAlliesState(PLY_AllyManager.allyState.activate);
    }
    
}
