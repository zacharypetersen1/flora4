﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ABI_MultiShot : ABI_ShotTypeStandard {

    public override void start()
    {
        initShot(6, .4f, 1.5f, false, PLY_Ammo.ammoTypes.red, 4);
    }

    protected override void doShot()
    {
        ABI_Nozzle.shootParticleChildren(ABI_Nozzle.nozzle.multiShot, 1);
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/twig_purify_spread", player.transform.position);
    }
}
