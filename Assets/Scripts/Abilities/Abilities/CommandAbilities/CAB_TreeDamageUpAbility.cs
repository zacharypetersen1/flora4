﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAB_TreeDamageUpAbility : MonoBehaviour {

    Collider myCollider;
	// Use this for initialization
	void Start () {
        myCollider = GetComponent<SphereCollider>();
        myCollider.enabled = false;
        myCollider.enabled = true;
    }
	
	// Update is called once per frame
	void Update () {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Minion")
        {
            ENT_Body body = other.gameObject.GetComponent<ENT_Body>();
            if (body.isAlly)
            {
                body.receive_combat_message(new CBI_CombatMessenger(0, 0, 0, new List<CBI_CombatMessenger.status>() { CBI_CombatMessenger.status.damageIncrease }, transform.position));
            }
        }
    }
}
