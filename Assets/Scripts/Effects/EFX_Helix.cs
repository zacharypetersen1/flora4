﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_Helix : MonoBehaviour {

    //repeat rate of circle
    public int frequency = 1;
    public float resolution = 20;
    public float amplitude = 1.0f;
    public float Zvalue = 0f;
    public bool left = false;
    public bool right = false;

	// Use this for initialization
	void Start () {
        createCircle();

    }

    void createCircle()
    {
        ParticleSystem ps = GetComponent<ParticleSystem>();
        var velocity = ps.velocityOverLifetime;
        velocity.enabled = true;
        velocity.space = ParticleSystemSimulationSpace.Local;
        ps.startSpeed = 0f;
        velocity.z = new ParticleSystem.MinMaxCurve(10.0f, Zvalue);

        
        AnimationCurve curveX = new AnimationCurve();
        for (int i = 0; i < resolution; i++)
        {
            float newtime = (i / (resolution - 1));
            float myValue = 0;
            if (left) {
                myValue = amplitude * Mathf.Sin(newtime * (frequency*2) * Mathf.PI);
            }
            else
            {
                myValue = amplitude * Mathf.Cos(newtime * (frequency * 2) * Mathf.PI);
            }
            curveX.AddKey(newtime, myValue);
        }
        velocity.x = new ParticleSystem.MinMaxCurve(10.0f, curveX);


        AnimationCurve curveY = new AnimationCurve();
        for (int i = 0; i < resolution; i++)
        {
            float newtime = (i / (resolution - 1));
            float myValue = 0;
            if (left)
            {
                myValue = amplitude * Mathf.Cos(newtime * (frequency * 2) * Mathf.PI);
            }
            else
            {
                myValue = amplitude * Mathf.Sin(newtime * (frequency * 2) * Mathf.PI);
            }

            curveY.AddKey(newtime, myValue);
        }
        velocity.y = new ParticleSystem.MinMaxCurve(10.0f, curveY);
    }
	
}
