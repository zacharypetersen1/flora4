﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_PulseMagic : MonoBehaviour {

    static EFX_PulseMagic singleton;
    public AnimationCurve offsetCurve, alphaCurve;
    public Material[] mats;
    int pulseType = 0;
    float curTime;
    public float pulseTime;
    bool pulsing = false;

    int debugIndex = 0;
    int debugFrame = 0;

	// Use this for initialization
	void Awake () {
        if (singleton)
        {
            Destroy(singleton);
        }
        singleton = this;
	}

    void Start()
    {
        initMats();
    }
	
	// Update is called once per frame
	void Update () {
        /*if((debugFrame++) % 60 == 0)
        {
            debugIndex = (debugIndex + 1) % 6;
            pulse(debugIndex);
        }*/

        if (pulsing)
        {
            curTime += Time.deltaTime;
            if(curTime >= pulseTime)
            {
                pulsing = false;
                updateMat(mats[pulseType], 0, 0);
            }

            float normTime = curTime / pulseTime;
            float curOffset = offsetCurve.Evaluate(normTime);
            float curAlpha = alphaCurve.Evaluate(normTime);
            updateMat(mats[pulseType], curAlpha, curOffset);
        }
	}

    public void updateMat(Material mat, float alpha, float offset)
    {
        mat.SetFloat("_OutlineSize", offset);
        mat.SetFloat("_Alpha", alpha);
    }

    public static void pulse(int type)
    {
        singleton.updateMat(singleton.mats[singleton.pulseType], 0, 0);
        singleton.pulsing = true;
        singleton.curTime = 0;
        singleton.pulseType = type;
    }

    public void initMats()
    {
        for(int i = 0; i < mats.Length; i++)
        {
            updateMat(mats[i], 0, 0);
        }
    }
}
