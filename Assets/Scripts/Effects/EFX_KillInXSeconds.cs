﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_KillInXSeconds : MonoBehaviour {

    public float timeToKill = 1;
	// Use this for initialization
	void Start () {
        Destroy(gameObject, timeToKill);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
