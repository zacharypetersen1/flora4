﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_HandTrails : MonoBehaviour {

    static EFX_HandTrails singleton;
    TrailRenderer left, right;

	// Use this for initialization
	void Awake () {
        if (singleton)
        {
            Destroy(singleton);
        }
        singleton = this;
        left = GameObject.Find("HandMagicEffectLeft").GetComponent<TrailRenderer>();
        right = GameObject.Find("HandMagicEffectRight").GetComponent<TrailRenderer>();
    }
	
    public static void setEnabled(bool state)
    {
        singleton.left.enabled = state;
        singleton.right.enabled = state;
    }
}
