﻿// Zach

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MAP_NatureMap1 : MonoBehaviour {

    static MAP_NatureMap1 singleton;
    public int density = 40;
    public int voronoiQuadrants = 6;
    int resolution = 512;
    public bool generateOnStart = true;
    MAP_VoronoiMap voronoiMap;
    Texture2D tex;
    public Texture2D rockTex;
    public Texture2D grassTex;
    public Texture2D corruptTex;
    public Texture2D[] grassLayersTex;
    public float[] grassLayersThresholds;
    //List<TFX_TextureEffect> grassEffects;
    //List<TFX_TextureEffect> stoneEffects;
    //List<TFX_TextureEffect> corruptEffects;
    Dictionary<MAP_NatureData.type, List<TFX_TextureEffect>> effects;

    //
    // Runs before start and update
    //
    void Awake()
    {
        singleton = this;

        effects = new Dictionary<MAP_NatureData.type, List<TFX_TextureEffect>>
        {
            {MAP_NatureData.type.grass  ,  new List<TFX_TextureEffect>()},
            {MAP_NatureData.type.rock   ,  new List<TFX_TextureEffect>()},
            {MAP_NatureData.type.corrupt,  new List<TFX_TextureEffect>()},
        };
        //grassEffects = new List<TFX_TextureEffect>();
        //stoneEffects = new List<TFX_TextureEffect>();
        //corruptEffects = new List<TFX_TextureEffect>();

        // attach grass effects
        //grassEffects.Add(new TFX_TextureBlender(grassLayersTex, grassLayersThresholds, 2f, 5));
        //grassEffects.Add(new TFX_PerlinShading(10, 0.04f));
        effects[MAP_NatureData.type.grass].Add(new TFX_TextureBlender(grassLayersTex, grassLayersThresholds, 2f, 5));
        effects[MAP_NatureData.type.grass].Add(new TFX_PerlinShading(10, 0.04f));

        // attach stone effects
        //stoneEffects.Add(new TFX_TextureApplier(rockTex, 5));
        effects[MAP_NatureData.type.rock].Add(new TFX_TextureApplier(rockTex, 5));

        // attatch corrupt effects
        //corruptEffects.Add(new TFX_TextureApplier(corruptTex, 5));
        //corruptEffects.Add(new TFX_PerlinShading(10, 0.04f));
        effects[MAP_NatureData.type.corrupt].Add(new TFX_TextureApplier(corruptTex, 5));
        effects[MAP_NatureData.type.corrupt].Add(new TFX_PerlinShading(10, 0.04f));
    }



    //
    // Use this for initialization
    //
    void Start () {
     
        if(transform.localScale.x != transform.localScale.y)
        {
            //throw new UnityException("X and Y scale of the ground object must be the same.");
        }

        if (generateOnStart)
        {
            voronoiMap = new MAP_VoronoiMap(density, voronoiQuadrants);
            updateTexture();
        }
	}
	


	//
	// Update is called once per frame
	//
	void Update () {
	
	}



    //
    // Gets texture using voronoi map
    //
    void updateTexture()
    {
        tex = new Texture2D(resolution, resolution);
        for(int x = 0; x < resolution; x++)
        {
            for(int y = 0; y < resolution; y++)
            {
                // get normalized position
                float pixelX = (float)x / resolution;
                float pixelY = (float)y / resolution;

                // get nature type from voronoi map
                MAP_NatureData.type pixelType = voronoiMap.getVoronoiType(pixelX, pixelY);

                // pull pixel value from corresponding texture map
                Color c = new Color();

                // apply effects to the pixel
                foreach (var effect in effects[pixelType])
                {
                    c = effect.alter(pixelX, pixelY, c);
                }
                tex.SetPixel(resolution - x, resolution - y, c);

                /*if (pixelType == MAP_NatureData.type.grass)
                {
                    foreach(var effect in grassEffects)
                    {
                        c = effect.alter(pixelX, pixelY, c);
                    }
                }
                else
                {
                    foreach (var effect in stoneEffects)
                    {
                        c = effect.alter(pixelX, pixelY, c);
                    }
                }
                tex.SetPixel(resolution - x, resolution - y, c);*/
            }
        }
        tex.Apply();

        gameObject.GetComponent<Renderer>().material.mainTexture = tex;
    }



    //
    // Transforms unity position into [0, 1) range for use with voronoi map
    //
    public Vector2 mapToVoronoi(float x, float y)
    {
        Vector2 vec = new Vector2();
        vec.x = x / (transform.localScale.x * 10);
        vec.y = y / (transform.localScale.y * 10);
        return vec;
    }



    //
    // Returns type of nature at position specified
    //
    public static MAP_NatureData.type natureTypeAtPos(float x, float y)
    {


        return singleton.voronoiMap.getVoronoiType(singleton.mapToVoronoi(x, y));

       /* Vector2 mappedLoc = singleton.mapToVoronoi(x, y);
        int resX = (int)Mathf.Floor(singleton.resolution * mappedLoc.x);
        int resY = (int)Mathf.Floor(singleton.resolution * mappedLoc.y);
        Debug.Log(singleton.tex.height);
        Debug.Log(mappedLoc + " " + resX + " " + resY);
        Color c = singleton.tex.GetPixel(resX, resY);
        c = new Color((float)Math.Round(c.r, 2), (float)Math.Round(c.g, 2), (float)Math.Round(c.b, 2));
        foreach (KeyValuePair<NatureData.type, Color> entry in NatureData.colorMap)
        {
            if(entry.Value == c)
            {
                return entry.Key;
            }
        }
        throw new UnityException("Could not find nature type to match color: " + c);*/
    }
    public static MAP_NatureData.type natureTypeAtPos(Vector3 position)
    {
        return natureTypeAtPos(position.x, position.y);
    }



    //
    // checks if point is within bounds of this nature map
    //
    public static bool inBounds(Vector3 loc)
    {
        return    loc.x > singleton.transform.position.x - singleton.transform.localScale.x * 5f
               && loc.y > singleton.transform.position.y - singleton.transform.localScale.y * 5f
               && loc.x < singleton.transform.position.x + singleton.transform.localScale.x * 5f
               && loc.y < singleton.transform.position.y + singleton.transform.localScale.y * 5f;
    }
}
