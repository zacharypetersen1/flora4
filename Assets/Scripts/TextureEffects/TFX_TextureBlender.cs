﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TFX_TextureBlender : TFX_TextureEffect {



    private Texture2D[] textures;       // textures to be mixed, index 0 is lowest
    private TFX_TextureApplier[] texAppliers;
    private float[] thresholds;         // thresholds of the textures
    float sprScalar;
    NOI_Noise noise;



    //
    // Constructor
    //
    public TFX_TextureBlender (Texture2D[] setTextures, float[] setThresholds, float spreadScalar, float textureScalar)
    {
        textures = setTextures;
        thresholds = setThresholds;
        sprScalar = spreadScalar;
        noise = new NOI_CompoundPerlin(new float[] { spreadScalar, spreadScalar * 2, spreadScalar * 4, spreadScalar * 8, spreadScalar * 16 }, 1);

        // initialize array of texture appliers
        texAppliers = new TFX_TextureApplier[textures.Length];
        for(int i = 0; i < textures.Length; i++)
        {
            texAppliers[i] = new TFX_TextureApplier(textures[i], textureScalar);
        }
    }

    

    //
    // Applies texture effect to single pixel
    //
    public override Color alter(float x, float y, Color initial)
    {
        return texAppliers[getLevel(x, y)].alter(x, y, initial);
    }



    //
    // determines which texture level this pixel is on
    //
    private int getLevel(float x, float y)
    {

        float value = noise.getValue(x, y);

        // determine which level
        for (int i = 0; i < thresholds.Length; i++)
        {
            if(value <= thresholds[i])
            {
                return i;
            }
        }

        return textures.Length-1;
    }
}
