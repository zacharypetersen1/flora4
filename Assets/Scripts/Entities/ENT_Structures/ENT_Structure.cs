﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_Structure : ENT_DungeonEntity {

    /// <summary>
    /// Reference to zone that contains the structure
    /// </summary>
    public Zone ParentZone;
    public int priority; // How important it is for the player to target the structure
    public string JSON_path; // Where to find JSON data for the structure object
    public GameObject twig; // Reference to player object
    public int ID; //ID for each structure so that minions can know where they spawned from
                   //public UIX_CameraFacingBillboard_Minion healthBar;

    protected ParticleSystem[] particleEffects;
    protected GameObject childBackgroundBar;
    public Collider attackableArea = null;

    Collider myCollider;
    public AIC_Zone myZone;

    public bool isGrandGaurdian = false;
    private bool isParticleSystemEnabled;
    // Use this for initialization
    public override void Start () {
        base.Start();
        isParticleSystemEnabled = true;
        //New health bars
        childBackgroundBar = UTL_Resources.cloneAtLocation("FloatingUI/BackgroundBar", transform.position);
        childBackgroundBar.transform.parent = transform;
        Collider col = GetComponent<Collider>();
        if (col != null)
        {
            childBackgroundBar.GetComponent<FUI_PurityBar>().offset = col.bounds.size.y + .5f;
        }
        childBackgroundBar.transform.localScale *= 2.0f;
        allyManager.addStructure(gameObject);
        CL = CL_MAX;
        twig = player;
        particleEffects = GetComponentsInChildren<ParticleSystem>();
        SetPSEnabled(false);
        State = "active";

        if (attackableArea == null)
        {
            Transform temp = transform.Find("AttackableArea");
            if (temp != null)
            {
                attackableArea = temp.GetComponent<Collider>();
            }
            else
            {
                Debug.LogWarning(gameObject.name + " is missing it's AttackableArea child, please attach one for melee attacks to work properly");
            }
        }

        if (isGrandGaurdian)
        {
            Purity = true;
            FUI_PurityBar hpBar = childBackgroundBar.GetComponent<FUI_PurityBar>();
            hpBar.transform.localScale *= 2;
            hpBar.transform.GetChild(1).GetComponent<SpriteRenderer>().color = new Color(0, 138, 1);
        }
        myZone = AIC_ZoneManager.putStructureInZone(gameObject);
    }

    void subStart()
    {
        // Implementation in derived classes
    }
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
        subUpdate();
	}

    void subUpdate()
    {
        // Implementation in derived classes
    }


    public void SpawnEnemy()
    {
        // Implementation in derived classes
    }

    //May not be necessary in this intermediate level class; implemented below this class in the hierarchy, interfaced above it.
    public override void activate()
    {

    }

    public override void on_delta_clarity()
    {
        //Old code
        //if (healthBar != null) {
        //    healthBar.updateHealthBar(CL_MAX, CL);
        //}
    }

    private void CheckForDeath()
    {
        if (CL > 0)
            return;

        if (State != "active")
            return;

        cleanse_self();
    }
    public override void inflict_damage(float DC)
    {
        base.inflict_damage(DC);
        CheckForDeath();
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        Transform[] children = transform.GetComponentsInChildren<Transform>();
        int childCount = children.Length;
        for (int j = 0; j <childCount; ++j)
        {
            Transform child = children[j];
            //Remove crystals
            if (child.name.StartsWith("StructureCrystal"))
            {
                //UnityEngine.Object.Destroy(child.gameObject);
                ParticleSystem[] partSystems = child.GetComponentsInChildren<ParticleSystem>();
                int partCount = partSystems.Length;
                for (int i = 0; i < partCount; ++i)
                {
                    ParticleSystem ps = partSystems[i];
                    if (ps.gameObject.name == "lightning")
                        ps.Stop();
                    else
                        ps.Play();
                }
                MeshRenderer mesh = child.GetComponent<MeshRenderer>();
                if (mesh)
                    mesh.enabled = false;
            }
        }
        if (isGrandGaurdian)
        {
            ENT_PlayerBody playerBody = GameObject.FindGameObjectWithTag("Player").GetComponent<ENT_PlayerBody>();
            playerBody.loseGame();
        }
        else
        {
            GameObject healthBar = gameObject.transform.Find("BackgroundBar(Clone)").gameObject;
            if (healthBar != null) Destroy(gameObject.transform.Find("BackgroundBar(Clone)").gameObject);
            allyManager.removeStructure(gameObject);
            if (!gameObject.name.Contains("ShieldTarget") && !SaveLoad.structuresDestroyed.Contains(gameObject.name))
                SaveLoad.structuresDestroyed.Add(gameObject.name);
        }
        myZone.removeStructureFromZone(gameObject);
    }

    public virtual void SetPSEnabled(bool enable)
    {
        if (enable != isParticleSystemEnabled)
        {
            isParticleSystemEnabled = enable;
            int psCount = particleEffects.Length;
            for (int i = 0; i < psCount; ++i)
            {
                ParticleSystem ps = particleEffects[i];
                if (enable)
                {
                    if (!(ps.gameObject.transform.parent && ps.gameObject.transform.parent.name.StartsWith("CPurityEffect")))
                        ps.Play();
                    //Debug.Log("Start playing paticle effects on building" + gameObject.name);
                }
                else
                {
                    ps.Stop();
                    //Debug.Log("Stopped playing paticle effects on building" + gameObject.name);
                }
            }
        }
    }

    public override Vector3 getNearestAttackablePoint(Vector3 originalPos)
    {
        if (attackableArea != null)
        {
            //print("using attackable area");
            return attackableArea.ClosestPointOnBounds(originalPos);
        }
        else return transform.position;
    }

    public void OnZoneStatusChange(bool nowEnabled)
    {
        //make sure disabled structures come after enabled structures in the list
        if (State == "cleansed")
            return;

        if (!purity && nowEnabled != gameObject.activeInHierarchy)
        {
            allyManager.removeStructure(gameObject);
            allyManager.addStructure(gameObject, true, nowEnabled);
        }
    }

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    try
    //    {
    //        CBI_CombatMessenger message = collision.gameObject.GetComponent<CBI_CombatMessenger>();
    //        receive_combat_message(message);
    //    }
    //    catch
    //    {
    //        Debug.Log("no combat message to recieve");
    //    }
    //}
}
