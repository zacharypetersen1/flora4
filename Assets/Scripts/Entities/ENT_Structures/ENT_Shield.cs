﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ENT_Shield : ENT_Structure {
    public ENT_DungeonEntity entity_to_shield;
    bool applied_shield;
    public Material[] shield_materials = new Material[2];
    GameObject shield_effect;
    //Uses degrees
    public float beamAngle;
    public int numPulses = 20;
    public float beamOffsetStart = 0;
    public float beamOffsetEnd = 0;
    private List<GameObject> pulses = new List<GameObject>();
    private float numSteps;
    private float pulseDelay; //in seconds
    private float timer = 0;

    public GameObject[] crystals;
    bool crystalsCleansed = false;
    public bool useAutoShieldScale = true;

    // Use this for initialization
    public override void Start () {
        base.Start();
        applied_shield = false;
        if (entity_to_shield)
        {
            numPulses = Mathf.Max(numPulses, Mathf.FloorToInt(Vector3.Distance(entity_to_shield.transform.position, gameObject.transform.position)));
            State = "active"; // Only activate if there is a target
            StartCoroutine(shield_entity());
        }  
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        //update_shield_entity();
        
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (State == "cleansed")
        {
            if (!crystalsCleansed) return;
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.08f, gameObject.transform.position.z);
            timer += 0.08f;
            if (timer >= 20)
            {
                Destroy(gameObject);
            }
            return;
        }
    }

    IEnumerator shield_entity()
    {
        if (!applied_shield && entity_to_shield != null)
        {
            if (entity_to_shield.shields <= 0)
            {
                entity_to_shield.shields += 1;
                applied_shield = true;
                shield_effect = UTL_Resources.cloneAsChild("Environment Effects/Shield/Shield", entity_to_shield.gameObject);
                Vector3 temp = shield_effect.transform.position;
                temp.z -= .01F;
                shield_effect.transform.position = temp; //reduces z-fighting
                if (useAutoShieldScale)
                    shield_effect.transform.localScale = Vector3.one;
            }
            else if (entity_to_shield.shields >= 1)
            {
                entity_to_shield.shields += 1;
                applied_shield = true;
                try
                {
                    foreach (Transform child in entity_to_shield.transform)
                    {
                        if (child.gameObject.tag == "shield")
                        {
                            shield_effect = child.gameObject;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError("no ParticleSystem Renderer");
                    throw e;
                }
            }
        }
        numSteps = Vector2.Distance(new Vector2(gameObject.transform.position.x, gameObject.transform.position.z),
            new Vector2(entity_to_shield.transform.position.x, entity_to_shield.transform.position.z));
        pulseDelay = numSteps / numPulses;
        for (int i = 0; i < numPulses; ++i)
        {
            GameObject pulse = UTL_Resources.cloneAtLocation("shieldbeam", transform.position);
            ENT_ShieldSkyPulse beamPulse = pulse.GetComponent<ENT_ShieldSkyPulse>();
            beamPulse.origin = this;
            beamPulse.target = entity_to_shield;
            beamPulse.delay = pulseDelay;
            pulses.Add(pulse);
            yield return new WaitForSeconds(pulseDelay);
        }
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        State = "cleansed";
        entity_to_shield.shields -= 1;
        applied_shield = false;
        //print("shields: " + entity_to_shield.shields);
        if (entity_to_shield.shields <= 0)
        {
            //print("Shield should be down. Shield_effect: " + shield_effect.name);
            Destroy(shield_effect);

        }
        /*
        var children = gameObject.transform.GetComponentInChildren<Transform>();
        foreach (Transform child in children)
        {
            //makes crystals attached to structure appear inert
            if (child.name.StartsWith("Crystal"))
            {
                child.GetComponent<MeshRenderer>().material = ghostMat;
            }
            //turns off MeshEffect on box if present
            else if (child.name == "BoxEffect")
            {
                child.gameObject.SetActive(false);
            }
            //turns off Lightning if any crystals with lightning effects are present
            else if (child.name == "Lightning")
            {
                Debug.Log("stopping lightning");
                child.gameObject.GetComponent<ParticleSystem>().Stop();
            } 
        }
        */
        StartCoroutine(WaitToPlayEffect(.2f, crystals));
        
    }

    IEnumerator WaitToPlayEffect(float waitTimer, GameObject[] crystals)
    {
        for (int i = 0; i < crystals.Length; ++i)
        {
            ParticleSystem lightning = crystals[i].transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
            if (lightning != null) lightning.Stop();

            GameObject cleanseEffect = crystals[i].transform.GetChild(1).gameObject;
            crystals[i].GetComponent<MeshRenderer>().enabled = false;
            if (cleanseEffect.name.Contains("CPurityEffect"))
            {   
                foreach (var ps in cleanseEffect.GetComponentsInChildren<ParticleSystem>())
                {
                    ps.Play();
                }
            }
            yield return new WaitForSeconds(waitTimer);
        }
        crystalsCleansed = true;
    }
}
