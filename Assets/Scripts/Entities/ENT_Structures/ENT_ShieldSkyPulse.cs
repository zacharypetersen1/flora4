﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_ShieldSkyPulse : MonoBehaviour {

    //public enum PulseState
    //{
    //    dormant,
    //    active
    //};

    Vector3 vel;
    public ENT_Shield origin;
    public ENT_DungeonEntity target;
    public Vector3 offsetStart;
    public Vector3 offsetEnd;
    public ParticleSystem emitter;
    public float angle;
    public float deltaY;
    public float numSteps;
    public float delay = 100f;
    private int step = 0;
    public float gravity; //amount object falls each frame.
    public string State = "dormant";
    public Vector3 scale = new Vector3(1,1,1);

   // public PulseState state;

	// Use this for initialization
	void Start () {
        if (target)
        {
            offsetStart = new Vector3(0, origin.beamOffsetStart, 0);
            offsetEnd = new Vector3(0, origin.beamOffsetEnd, 0);
            emitter = gameObject.GetComponent<ParticleSystem>();
            reset();
        }
    }
	
	// Update is called once per frame
	void FixedUpdate() {
        step++;
        gameObject.transform.position += vel;
        vel.y -= gravity;
        if(step >= numSteps + delay)
        {
            reset();
            emitter.Play();
        }
    }

    void Update()
    {
        if (UTL_Math.sqrDistance(gameObject.transform.localScale, scale) > 0.01f)
        {
            gameObject.transform.localScale = Vector3.Lerp(gameObject.transform.localScale, scale, 0.3f);
        }
        else
        {
            scale = new Vector3(Random.Range(1f, 5f), Random.Range(1f, 5f), Random.Range(1f, 5f));
        }
    }


    /// <summary>
    /// Given a specific apex height, calculates the trajectory for an object affected by gravity,
    /// given that its start position is the shield generator structure, and the end position is
    /// the generator's enitity_to_shield
    /// </summary>
    private Vector3 calculateTrajectory()
    {
        Vector3 startPos = origin.transform.position + offsetStart;
        Vector3 endPos = target.transform.position + offsetEnd;
        Vector3 toTarget = endPos - startPos;
        toTarget.y = 0;
        //calculate numSteps
        numSteps = Vector2.Distance(new Vector2(startPos.x, startPos.z), new Vector2(endPos.x, endPos.z));
        //calculate deltaY
        deltaY = endPos.y - startPos.y;
        //check for change in angle
        angle = origin.beamAngle;
        toTarget.Normalize();
        toTarget.y = 1/(Mathf.Cos(angle*2*Mathf.PI/360));
        gravity = toTarget.y / (numSteps/2);

        return toTarget;
    }

    private void reset()
    {
        if (origin == null || origin.State == "cleansed" || target == null)
        {
            Destroy(gameObject);
        }
        else
        {
            gameObject.transform.position = origin.transform.position + offsetStart;
            vel = calculateTrajectory();
            vel.y += deltaY / numSteps;
            step = 0;
            emitter.Play();
        }
    }
}
