﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_DestructableCrystal : ENT_Structure
{
    public ParticleSystem lightning;
    public GameObject cleanse_effect;
    ParticleSystem[] cleanse_PS;

    public override void Start()
    {
        base.Start();
        Purity = false;
        childBackgroundBar.GetComponent<FUI_PurityBar>().offset = GetComponent<Collider>().bounds.size.y + 2f;
        cleanse_PS = cleanse_effect.GetComponentsInChildren<ParticleSystem>();
        particleEffects = new ParticleSystem[] { lightning };
        State = "active";
        if (DBG_DebugOptions.Get().destroyAllCrystals)
        {
            cleanse_self();
        }
    }

    public override void cleanse_self()
    {
        base.cleanse_self();
        State = "cleansed";
        int psCount = cleanse_PS.Length;
        for (int i = 0; i < psCount; ++i)
        {
            ParticleSystem ps = cleanse_PS[i];
            ps.Play();
        }
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<Collider>().enabled = false;
        Transform twigBlockerXform = gameObject.transform.FindChild("TwigBlocker");
        if (twigBlockerXform)
        {
            Collider twigCol = twigBlockerXform.gameObject.GetComponent<Collider>();
            if (twigCol)
                twigCol.enabled = false;
        }


        Destroy(gameObject, 1);
    }
}
