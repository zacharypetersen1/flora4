﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_ConfusionAbility : ENT_MinionAbility
{

    private GameObject confusionAbility;

    protected override void _allyAbility()
    {
        base._allyAbility();
        //print("allyAbility entered");
        confusionAbility = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/ConfusionAbility/confusionAbility", gameObject.transform.position);
        confusionAbility.transform.rotation = Quaternion.Euler(new Vector3(-90, 0, 0));
        confusionAbility.transform.position += Vector3.up * 1.25f;
        confusionAbility.GetComponent<ABI_ConfusionAbility>().Purity = true;
    }

    protected override void _enemyAbility()
    {
        base._enemyAbility();
        confusionAbility = UTL_Resources.cloneAtLocation("Abilities/MinionAttacks/ConfusionAbility/enemyConfusionAbility", gameObject.transform.position);
        confusionAbility.transform.rotation = Quaternion.Euler(new Vector3(-90, 0, 0));
        confusionAbility.transform.position += Vector3.up * 1.25f;
        confusionAbility.GetComponent<ABI_ConfusionAbility>().Purity = false;
    }

    public override void endAllyAbility()
    {
        //print("endAllyAbility entered");
        base.endAllyAbility();
        //Debug.Log(gameObject.GetComponent<ENT_Body>().IsAbilityActive);
        //Destroy(slowAbility);

        ENT_Body body = gameObject.GetComponent<ENT_Body>();
        //body.CurrentTree = body.LastTree;
    }

    public override void endEnemyAbility()
    {
        base.endEnemyAbility();
        //Destroy(slowAbility);
    }
}
