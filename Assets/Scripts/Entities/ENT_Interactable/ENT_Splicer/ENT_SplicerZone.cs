﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ENT_SplicerZone : MonoBehaviour {

    private ENT_SplicerController controller;
    public List<ENT_SplicerController.splicerTicket> tickets;
    private List<ENT_SplicerController.splicerTicket> removeList;
    float radius;

    void Awake()
    {
        controller = transform.parent.GetComponentInChildren<ENT_SplicerController>();
        tickets = new List<ENT_SplicerController.splicerTicket>();
        removeList = new List<ENT_SplicerController.splicerTicket>();
        radius = GetComponent<SphereCollider>().radius;
    }

    public void OnTriggerStay(Collider other)
    {
        foreach(var ticket in tickets)
        {
            if (ticket.readyToSplice)
            {
                if (other.gameObject == ticket.minion0)
                {
                    ticket.minion0InPos = true;
                }
                if (other.gameObject == ticket.minion1)
                {
                    ticket.minion1InPos = true;
                }
            }
        }
        
    }

    public void Update()
    {
        foreach (var ticket in tickets)
        {
            if (ticket.readyToSplice && (ticket.minion0InPos && ticket.minion1InPos))
            {
                /*
                ENT_Body body0 = ticket.minion0.GetComponent<ENT_Body>();
                ENT_Body body1 = ticket.minion1.GetComponent<ENT_Body>();
                if (body0 == null || body1 == null)
                {
                    removeList.Add(ticket);
                    if (body0 != null)
                    {
                        controller.abortSplice(body0.gameObject);
                    }
                    else if (body1 != null)
                    {
                        controller.abortSplice(body1.gameObject);
                    }
                }
                body0.isGhost = true;
                body0.cleanse_self();
                body1.isGhost = true;
                body1.cleanse_self();
                Vector3 position;
                int navmeshMask = 1 << NavMesh.GetAreaFromName("Walkable");
                NavMeshHit hit;
                float distToCheck = 2;
                do
                {
                    Vector3 point = Random.insideUnitSphere;
                    float dist = Random.Range(0F, radius * 0.5F);
                    position = gameObject.transform.position + (point * dist) + (transform.forward * 5);
                    position = UTL_Dungeon.snapPositionToTerrain(position);
                    distToCheck *= 2;
                } while (!NavMesh.SamplePosition(position, out hit, distToCheck, navmeshMask));
                position = hit.position;
                //position = GameObject.Find("SplicerSpawn").transform.position;
                //print("position: " + position);
                UTL_Dungeon.spawnMinion(controller.result, position, true);
                removeList.Add(ticket);
                */
            }
        }
        foreach (var ticketToRemove in removeList)
        {
            tickets.Remove(ticketToRemove);
        }
        removeList.Clear();
    }
}
