﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENT_KangarooPaw : ENT_Interact
{

    public override void activate()
    {
        base.activate();
        GameObject hit = UTL_Resources.cloneAtLocation("Environment Effects/Hit", gameObject.transform.position);
        hit.transform.position = new Vector3(hit.transform.position.x, hit.transform.position.y, hit.transform.position.z - 3);
    }
}
