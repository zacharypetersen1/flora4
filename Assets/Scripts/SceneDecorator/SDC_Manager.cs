﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SDC_Manager : MonoBehaviour {



    [Header("Color Controls")]
    [Space(5)]
    [Header("Fog")]
    public Texture2D fogTexture;
    public Gradient fogGradient;
    [Space(5)]
    [Header("Grass")]
    public Texture2D grassTexture;
    public Gradient grassGradient;

    [Space(40)]

    [Header("Nature object generator")]
    public int numObjects = 200;
    public bool useRandScale = true;
    public float minScale = 0.8f;
    public float maxScale = 2f;
    public string assetPath = "Environment/Trees/Acacia_Trees";
    public Texture2D decoratorText;
    public Color maskColor;
    public float xPosMax = 640;
    public float yPosMax = 640;

    //
    //start function
    //Harold added this
    public void Start()
    {
        updateColors();
    }



    //
    // Updates the colors of the scene
    //
    public void updateColors()
    {
        // update fog texture
        for(int x = 0; x < fogTexture.width; x++)
        {
            float time = (float)x / fogTexture.width;
            Color col = fogGradient.Evaluate(time);
            fogTexture.SetPixel(x, 0, col);
            
        }
        fogTexture.Apply();
    }



    //
    // 
    //
    public void populateScene()
    {
        GameObject[] decoratorArr = Resources.LoadAll<GameObject>(assetPath);

        for (int i = 0; i < numObjects; i++)
        {
            float x = Random.Range(0, xPosMax);
            float y = Random.Range(0, yPosMax);
            /*print(x);
            print(y);
            print(decoratorText.GetPixel(x, y));
            print(maskColor);
            print(decoratorText.GetPixel(x, y).Equals(maskColor));*/
            if (decoratorText.GetPixel((int)x, (int)y).Equals(maskColor))
            {
                loadSingleDecorator(getRandObject(decoratorArr), x, y);
            }
        }
    }



    //
    // Destroys all types of decorators in the scene
    //
    public void cleanUpScene()
    {
        foreach (var obj in GameObject.FindGameObjectsWithTag("decorator"))
        {
            DestroyImmediate(obj);
        }
    }



    //
    // Loads a single decorator into the scene at a specific position with a random rotation
    //
    public void loadSingleDecorator(GameObject decorator, float x, float y)
    {
        GameObject temp = (GameObject)MonoBehaviour.Instantiate(decorator);
        temp.transform.position = UTL_Dungeon.snapPositionToTerrain(new Vector3(x, y));
        temp.gameObject.tag = "decorator";

        // apply scale if marked
        if (useRandScale)
        {
            temp.transform.localScale *= minScale + (maxScale - minScale) * Random.Range(0f, 1f);
        }

        temp.transform.Rotate(-90, 0, 0);
    }



    //
    // Selects a single GameObject from an array at random
    //
    public GameObject getRandObject(GameObject[] arr)
    {
        return arr[Random.Range(0, arr.Length)];
    }
}
