﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MUS_Settings : MonoBehaviour {

    FMOD.Studio.Bus Master;
    FMOD.Studio.Bus Minions;
    FMOD.Studio.Bus Music;
    FMOD.Studio.Bus SFX;
    FMOD.Studio.Bus UI;

    float master_volume = 1f;
    float minions_volume = 1f;
    float music_volume = 1f;
    float sfx_volume = 1f;
    float ui_volume = 1f;


    // Use this for initialization
    void Start () {
        Master = FMODUnity.RuntimeManager.GetBus("Bus:/");
        Minions = FMODUnity.RuntimeManager.GetBus("Bus:/Minions");
        Music = FMODUnity.RuntimeManager.GetBus("Bus:/Music");
        SFX = FMODUnity.RuntimeManager.GetBus("Bus:/SFX");
        UI = FMODUnity.RuntimeManager.GetBus("Bus:/UI");
    }
	
	void Update () {
        Master.setVolume(master_volume);
        Minions.setVolume(minions_volume);
        Music.setVolume(music_volume);
        SFX.setVolume(sfx_volume);
        UI.setVolume(ui_volume);
	}

    // Update volume levels through bus

    public void updateMasterVolume(float newMasterVolume)
    {
        master_volume = newMasterVolume;
    }

    public void updateMinionsVolume(float newMinionsVolume)
    {
        minions_volume = newMinionsVolume;
    }

    public void updateMusicVolume(float newMusicVolume)
    {
        music_volume = newMusicVolume;
    }

    public void updateSFXVolume(float newSFXVolume)
    {
        sfx_volume = newSFXVolume;
    }

    public void updateUIVolume(float newUIVolume)
    {
        ui_volume = newUIVolume;
    }
}
