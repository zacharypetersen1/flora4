﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFX_PulseManager: MonoBehaviour
{
    static EFX_PulseManager singleton;
    static bool isOn = false;
    int curPulseIndex = 0;
    bool[] isNearby = new bool[6];
    float curPulseTime = 0, maxPulseTime = 1.4f;

    void Awake()
    {
        if (singleton)
        {
            Destroy(singleton);
        }
        singleton = this;
    }

    void Start()
    {

    }

    void Update()
    {
        if (isOn)
        {
            curPulseTime -= TME_Manager.getDeltaTime(TME_Time.type.player);
            if(curPulseTime <= 0)
            {
                pulse();
                print(isNearby[0] + " " + isNearby[1] + " " + isNearby[2] + " " + isNearby[3] + " " + isNearby[4] + " " + isNearby[5]);
                print(curPulseIndex);
                curPulseTime += maxPulseTime;
            }
        }
    }

    public static void clearAll()
    {
        for(int i = 0; i < 6; i++)
        {
            singleton.isNearby[i] = false;
        }
    }

    public static void markAsNearby(ENT_Body.monsterTypes type)
    {
        singleton.isNearby[(int)type] = true;
    }

    public static void updateHandMagic()
    {
        for (int i = 0; i < 6; i++)
        {
            if (singleton.isNearby[i])
            {
                turnOn();
                return;
            }
        }
        turnOff();
    }

    static void turnOn()
    {
        if (!isOn)
        {
            singleton.curPulseTime = 0;
            isOn = true;
        }
    }

    static void turnOff()
    {
            isOn = false;
    }

    void shiftPulseType()
    {
        for(int i = 1; i < 6; i++)
        {
            int temp = (curPulseIndex + i) % 6;
            if (isNearby[temp])
            {
                curPulseIndex = temp;
                break;
            }
        }
    }

    void pulse()
    {
        shiftPulseType();
        EFX_PulseMagic.pulse(curPulseIndex);
    }
}
