﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLY_SproutDetector : MonoBehaviour {

    public static bool sproutInRange
    {
        get { return _sproutsInRange.Count > 0; }
    }

    public static void RemoveFromNearbySprouts(GameObject sprout)
    {
        _sproutsInRange.Remove(sprout);
    }


    static List<GameObject> _sproutsInRange;
    public static IList<GameObject> sproutsInRange
    {
        get { return _sproutsInRange.AsReadOnly(); }
    }

    ENT_PlayerBody playerBody;
    PLY_AllyManager allyManager;
    RezAvailabilityManager ram;

    List<ENT_Remnant> toRez;


    static SproutSorter sproutSorter;

    private void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        playerBody = player.GetComponent<ENT_PlayerBody>();
        allyManager = player.GetComponent<PLY_AllyManager>();
        ram = new RezAvailabilityManager();
        sproutSorter = new SproutSorter();
        sproutSorter.player = player;
        _sproutsInRange = new List<GameObject>();
        toRez = new List<ENT_Remnant>();
    }

    private void Sort()
    {
        _sproutsInRange.Sort(sproutSorter);
    }

    private void UpdateParticleSystems()
    {
        Sort();
        int sproutCount = _sproutsInRange.Count;
        ram.Reset();
        ram.availableMinions = allyManager.MaxFollowers - allyManager.allyCount - allyManager.wateredCount;
        ram.availableSlow = allyManager.MaxConfusion - allyManager.currSlow - allyManager.wateredCounts[ENT_Body.monsterTypes.slowMinion];
        ram.availableConfusion = allyManager.MaxConfusion - allyManager.currConfusions - allyManager.wateredCounts[ENT_Body.monsterTypes.confusionMinion];
        ram.availableNecro = allyManager.MaxNecro - allyManager.currNecro - allyManager.wateredCounts[ENT_Body.monsterTypes.necroMinion];
        EFX_PulseManager.clearAll();
        for (int i = 0; i < sproutCount; ++i)
        {
            GameObject sprout = _sproutsInRange[i];
            ENT_Remnant remnant = sprout.GetComponent<ENT_Remnant>();
            if (ram.IsAvailable(remnant))
            {
                EFX_PulseManager.markAsNearby(remnant.entity_type);
                remnant.UpdateParticleSystem();
                ram.MarkUsed(remnant);
            }
            else
            {
                remnant.ShutOffParticles();
            }
        }
        EFX_PulseManager.updateHandMagic();
    }

    private void Update()
    {
        UpdateParticleSystems();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "remnant")
        {
            if (playerBody.unlocked_types.Contains(other.GetComponent<ENT_Remnant>().entity_type))
            {
                _sproutsInRange.Add(other.transform.root.gameObject);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "remnant")
        {
            other.GetComponent<ENT_Remnant>().ShutOffParticles();
            _sproutsInRange.Remove(other.gameObject);
        }
    }

    public void DoRez()
    {
        Sort();
        int sproutCount = _sproutsInRange.Count;
        ram.Reset();
        ram.availableMinions = allyManager.MaxFollowers - allyManager.allyCount - allyManager.wateredCount;
        ram.availableSlow = allyManager.MaxConfusion - allyManager.currSlow - allyManager.wateredCounts[ENT_Body.monsterTypes.slowMinion];
        ram.availableConfusion = allyManager.MaxConfusion - allyManager.currConfusions - allyManager.wateredCounts[ENT_Body.monsterTypes.confusionMinion];
        ram.availableNecro = allyManager.MaxNecro - allyManager.currNecro - allyManager.wateredCounts[ENT_Body.monsterTypes.necroMinion];
        toRez.Clear();
        for (int i = 0; i < sproutCount; ++i)
        {
            GameObject sprout = _sproutsInRange[i];
            ENT_Remnant remnant = sprout.GetComponent<ENT_Remnant>();
            if (ram.IsAvailable(remnant))
            {
                //remnant.Rez();
                toRez.Add(remnant);
                ram.MarkUsed(remnant);
            }
            if (ram.availableMinions <= 0)
            {
                break;
            }
        }

        int toRezCount = toRez.Count;
        for (int i = 0; i < toRezCount; ++i)
        {
            ENT_Remnant remnant = toRez[i];
            remnant.Rez();
        }
    }

    private class RezAvailabilityManager
    {
        public int availableMinions;
        public int availableSlow;
        public int availableConfusion;
        public int availableNecro;

        public void Reset()
        {
            availableMinions = 0;
            availableSlow = 0;
            availableConfusion = 0;
            availableNecro = 0;
        }

        public bool IsAvailable(ENT_Remnant remnant)
        {
            if (availableMinions <= 0)
                return false;

            switch (remnant.entity_type)
            {
                case ENT_DungeonEntity.monsterTypes.confusionMinion:
                    return availableConfusion > 0;
                case ENT_DungeonEntity.monsterTypes.slowMinion:
                    return availableSlow > 0;
                case ENT_DungeonEntity.monsterTypes.necroMinion:
                    return availableNecro > 0;
                default:
                    return true;
            }
        }

        public void MarkUsed(ENT_Remnant remnant)
        {
            --availableMinions;

            switch (remnant.entity_type)
            {
                case ENT_DungeonEntity.monsterTypes.confusionMinion:
                    --availableConfusion;
                    break;
                case ENT_DungeonEntity.monsterTypes.slowMinion:
                    --availableSlow;
                    break;
                case ENT_DungeonEntity.monsterTypes.necroMinion:
                    --availableNecro;
                    break;
            }
        }
    }

    public class SproutSorter : IComparer<GameObject>
    {
        public GameObject player;

        public int Compare(GameObject a, GameObject b)
        {
            float distA = UTL_Math.sqrDistance(player.transform.position, a.transform.position);
            float distB = UTL_Math.sqrDistance(player.transform.position, b.transform.position);
            if (distA < distB)
                return -1;
            else if (distA > distB)
                return 1;
            else
                return 0;
        }
    }
}
