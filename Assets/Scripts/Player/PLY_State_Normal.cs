﻿// Zach

using System;
using UnityEngine;

public class PLY_State_Normal : PLY_MoveState
{



    //
    // runs when entering state
    //
    public override void StateEnter()
    {
        base.StateEnter();
    }



    //
    // runs once per frame
    //
    public override void StateUpdate()
    {
        // Nothing
    }



    //
    // runs once per frame at fixed intervals
    //
    public override void StateFixedUpdate()
    {
        //if (!ABI_Purify.busyRezing)
            motor.move(CAM_Camera.applyDollyRotZ(INP_PlayerInput.get2DAxis("Movement", true)));
        /*else
        {
            motor.setRotation(UTL_Math.vec3ToVec2(ABI_Purify.entityToPurify.transform.position - transform.position));
        }*/
    }
}
