﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENV_SproutPatch : MonoBehaviour {

    private static int _globalSproutCount;
    private static int _globalMaxSprouts = 200;

    public static int globalMaxSprouts
    {
        get { return _globalMaxSprouts; }
        set { _globalMaxSprouts = value; }
    }
    public static int globalSproutCount
    {
        get { return _globalSproutCount; }
    }

    public int maxSprouts = 10;
    public int spawnAtStart = 5;
    private int _sproutCount;
    public List<ENT_DungeonEntity.monsterTypes> sproutTypes;
    public float spawnTime;
    float timer;
    private SphereCollider col;
    private float radius;
    
    public int sproutCount
    {
        get { return _sproutCount; }
        
    }

	// Use this for initialization
	void Start () {
        GameObject sphere = transform.GetChild(0).gameObject;
        if (sphere != null)
            Destroy(sphere);
        col = GetComponent < SphereCollider>();
        if (col == null)
        {
            Debug.LogWarning("Error: " + name + " doesn't have a sphereCollider attached to it. Flora Sprouts won't spawn here");
            radius = 1.0f;
        }
        else
        {
            radius = col.radius;
            Destroy(col);
        }

        if (sproutTypes == null || sproutTypes.Count == 0)
        {
            sproutTypes = new List<ENT_DungeonEntity.monsterTypes>()
            {
                ENT_DungeonEntity.monsterTypes.flowerMinion,
                ENT_DungeonEntity.monsterTypes.mushroomMinion,
                ENT_DungeonEntity.monsterTypes.treeMinion
            };
        }
        spawnAtStart = Mathf.Clamp(spawnAtStart, 0, maxSprouts);
        for (int i = 0; i < spawnAtStart; ++i)
        {
            if (shouldSpawnSprout(true)) spawnSprout();
        }
    }
	
	// Update is called once per frame
	void Update () {
        timer += TME_Manager.getDeltaTime(TME_Time.type.enemies);
        if (shouldSpawnSprout()) spawnSprout();
	}

    public bool shouldSpawnSprout(bool ignoreTimer = false)
    {
        return ((ignoreTimer || timer >= spawnTime)
            && sproutCount < maxSprouts
            && globalSproutCount < globalMaxSprouts); 
    }

    public void spawnSprout()
    {
        ENT_DungeonEntity.monsterTypes typeToSpawn = sproutTypes[Random.Range(0, sproutTypes.Count)];
        float distance = Random.Range(0, radius);
        Vector2 rPoint = Random.insideUnitCircle * distance;
        Vector3 point = transform.position;
        point.x += rPoint.x;
        point.z += rPoint.y;
        if (UTL_Dungeon.putPointOnNavmesh(ref point, distance))
        {
            UTL_Dungeon.spawnSprout(typeToSpawn, point, this);
            _sproutCount++;
            //_globalSproutCount++;
            timer = 0;
        }
    }

    public static void decrementGlobalSproutCount()
    {
        _globalSproutCount--;
    }

    public void decrementSproutCount()
    {
        _sproutCount--;
    }
}
