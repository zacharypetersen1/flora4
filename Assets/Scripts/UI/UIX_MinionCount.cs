﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class UIX_MinionCount : MonoBehaviour
{

    public float x_offset = -0.41f;
    public float y_offset = 0;
    public float z_offset = 0; // Use to arrange multiple elements, lower is closer
    public GameObject text_object;
    public ENT_DungeonEntity.monsterTypes monstertype; // Monster type to count
    private TextMeshPro text;
    private SEL_Manager sel_manager;
    private Vector3 position;


    void Start()
    {
        if (text_object)
        {
            position = new Vector3(x_offset, y_offset, z_offset);
            if (text_object)
            {
                text = text_object.GetComponent<TextMeshPro>();
            }
            var cam_r = transform.parent.transform.rotation;
            sel_manager = GameObject.Find("Scripts").GetComponent<SEL_Manager>();
            text_object.transform.rotation = transform.parent.rotation;
            text_object.transform.position = transform.position;
            text_object.transform.localPosition += new Vector3(x_offset, y_offset, z_offset);
        } else
        {
            Debug.Log("UIX_MinionCount: text object is not specified");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (text_object)
        {
            text_object.transform.rotation = transform.parent.rotation;
            text_object.transform.position = transform.position;
            text_object.transform.localPosition += new Vector3(x_offset, y_offset, z_offset);
            Pair<Dictionary<ENT_DungeonEntity.monsterTypes, int>, Dictionary<ENT_DungeonEntity.monsterTypes, int>> selectedCounts = sel_manager.getSelectedByType();
            int countSelected = 0;
            if (selectedCounts.value1.ContainsKey(monstertype)) countSelected = selectedCounts.value1[monstertype];
            int countTotal = 0;
            if (selectedCounts.value2.ContainsKey(monstertype)) countTotal = selectedCounts.value2[monstertype];
            text.SetText(countSelected + " / " + countTotal);
        }
    }
}