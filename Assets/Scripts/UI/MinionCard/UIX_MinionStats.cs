﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIX_MinionStats : MonoBehaviour {

    static UIX_MinionStats singleton;
    GameObject hpForeground, descriptions, hashes, atkRanks, minionName;
    static string curMinionName = "Tree";
    public static ENT_Body curMinion;
    Transform widgets;

    // Stores references to the UI widgets that must be toggled for a type of minion
    class WidgetRefs
    {
        public GameObject atkRankObj, hpBarObj, descriptionObj;
        public WidgetRefs(GameObject setAtk, GameObject setHp, GameObject setDescr)
        {
            atkRankObj = setAtk;
            hpBarObj = setHp;
            descriptionObj = setDescr;
        }
        public void SetAllActive(bool value)
        {
            atkRankObj.SetActive(value);
            hpBarObj.SetActive(value);
            descriptionObj.SetActive(value);
        }
    }

    // Maps the type of the minion to correlating string name
    static Dictionary<ENT_DungeonEntity.monsterTypes, string> typeToName = new Dictionary<ENT_DungeonEntity.monsterTypes, string>()
    {
        {ENT_DungeonEntity.monsterTypes.flowerMinion, "Flower"},
        {ENT_DungeonEntity.monsterTypes.treeMinion, "Tree"},
        {ENT_DungeonEntity.monsterTypes.mushroomMinion, "Mushroom"},
        {ENT_DungeonEntity.monsterTypes.confusionMinion, "Bell"},
        {ENT_DungeonEntity.monsterTypes.necroMinion, "Necro"},
        {ENT_DungeonEntity.monsterTypes.slowMinion, "Sticky"}
    };

    // Maps string name to object storing references to related UI widgets
    static Dictionary<string, WidgetRefs> nameToRefs;

    void Start()
    {
        singleton = this;
        widgets = transform.FindChild("Widgets");
        hpForeground = widgets.FindChild("HP_Bar").FindChild("HP_Foreground").gameObject;
        descriptions = widgets.FindChild("Descriptions").gameObject;
        hashes = widgets.FindChild("HP_Bar").FindChild("Hashes").gameObject;
        atkRanks = widgets.FindChild("Atk_Bar").FindChild("AtkRanks").gameObject;
        minionName = widgets.FindChild("MinionName").gameObject;

        // Construct all references from minion name to UI widgets
        nameToRefs = new Dictionary<string, WidgetRefs>();
        nameToRefs.Add("Flower", new WidgetRefs(
                atkRanks.transform.FindChild("AtkRank_2").gameObject,
                hashes.transform.FindChild("HP10_Hash").gameObject,
                descriptions.transform.FindChild("Flower").gameObject
        ));
        nameToRefs.Add("Mushroom", new WidgetRefs(
                atkRanks.transform.FindChild("AtkRank_2").gameObject,
                hashes.transform.FindChild("HP20_Hash").gameObject,
                descriptions.transform.FindChild("Mushroom").gameObject
        ));
        nameToRefs.Add("Tree", new WidgetRefs(
                atkRanks.transform.FindChild("AtkRank_2").gameObject,
                hashes.transform.FindChild("HP30_Hash").gameObject,
                descriptions.transform.FindChild("Tree").gameObject
        ));
        nameToRefs.Add("Necro", new WidgetRefs(
                atkRanks.transform.FindChild("AtkRank_1").gameObject,
                hashes.transform.FindChild("HP50_Hash").gameObject,
                descriptions.transform.FindChild("Necro").gameObject
        ));
        nameToRefs.Add("Sticky", new WidgetRefs(
                atkRanks.transform.FindChild("AtkRank_3").gameObject,
                hashes.transform.FindChild("HP25_Hash").gameObject,
                descriptions.transform.FindChild("Sticky").gameObject
        ));
        nameToRefs.Add("Bell", new WidgetRefs(
                atkRanks.transform.FindChild("AtkRank_3").gameObject,
                hashes.transform.FindChild("HP25_Hash").gameObject,
                descriptions.transform.FindChild("Bell").gameObject
        ));

        // Turn all the stuff off until a minion is selected
        for(int i = 0; i < atkRanks.transform.childCount; i++)
        {
            atkRanks.transform.GetChild(i).gameObject.SetActive(false);
        }

        for (int i = 0; i < hashes.transform.childCount; i++)
        {
            hashes.transform.GetChild(i).gameObject.SetActive(false);
        }

        for (int i = 0; i < descriptions.transform.childCount; i++)
        {
            descriptions.transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    // Logic to make sure top selected minion is showing stats
    void Update()
    {
        GameObject curSelMinionOBJ = SEL_Manager.GetTopSelectedMinion();
        if (curSelMinionOBJ != null)
        {
            ENT_Body curSelectedMinion = curSelMinionOBJ.GetComponent<ENT_Body>();
            if(curSelectedMinion != curMinion)
            {
                setToMinion(curSelectedMinion);
            }
            else
            {
                drawHPBar(curSelectedMinion);
            }
        }
        else
        {
            setVisibility(false);
            curMinion = null;
        }
    }

    // Turns on UI elements and sets to new minion
    public static void setToMinion(ENT_Body minion)
    {
        setVisibility(true);

        // Turn off previous widgets
        nameToRefs[curMinionName].SetAllActive(false);

        // Store cur minion info
        curMinion = minion;
        curMinionName = typeToName[minion.getMyType()];

        // Turn on/configure ui widgets
        singleton.widgets.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/MinionCards/" + curMinionName);
        singleton.minionName.GetComponent<TextMeshProUGUI>().text = curMinion.minionName;
        nameToRefs[curMinionName].SetAllActive(true);
        drawHPBar(minion);
    }

    // Scales health bar
    public static void drawHPBar(ENT_Body minion)
    {
        float hpScalar = minion.CL / minion.CL_MAX;
        RectTransform t = singleton.hpForeground.GetComponent<RectTransform>();
        t.localScale = new Vector3(hpScalar, 1, 1);
    }

    // Sets the whole minion status UI bar visibility
    public static void setVisibility(bool value)
    {
        singleton.widgets.gameObject.SetActive(value);
    }
}
