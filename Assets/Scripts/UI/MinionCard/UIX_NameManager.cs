﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UIX_NameManager{

    class randStringList
    {
        List<string> strings;
        int index;

        public randStringList(List<string> setStrings)
        {
            strings = setStrings;
            strings.Shuffle();
        }

        public void loadFromSave(List<string> setStrings, int setIndex)
        {
            strings = setStrings;
            index = setIndex;
        }

        public string getString()
        {
            string ret = strings[index];
            incriment();
            return ret;
        }

        void incriment()
        {
            index++;
            if(index == strings.Count)
            {
                strings.Shuffle();
                index = 0;
            }
        }

        public int stringsLeft()
        {
            return strings.Count - index;
        }
    }

    static randStringList genNames = new randStringList(new List<string>()
    {
        "Johann Branch",
	    "Julius Sunflower",
	    "Photo Cynthia",
	    "Winstem Churchill",
	    "Abraherb Leafcoln",
	    "George Woodington",
	    "Mahatma Garden",
	    "Isaac Nectar",
	    "Alfalfa Einstein",
	    "Tree Musketeer",
	    "Morgan Treeman",
	    "Roman Plantsky",
	    "Plant Armstrong",
	    "Cactus Everdeen",
	    "Leaf Erikson",
	    "Johnny Appleseed",
	    "Marilyn Mongrow",
	    "Elvis Parsley",
	    "Hammer Thyme",
	    "George Bloomy",
	    "Mark Bulberg",
	    "Lavender Hollifield",
	    "Hot Pine Bling",
	    "Herb Your Enthusiasm",
	    "Nicholas Sage",
	    "Roman Plantski",
	    "Adam Levine",
	    "Briar Tuck",
	    "Leafy",
	    "Spiny",
	    "Bud",
	    "Cellulose",
	    "Rhizome",
	    "Cotyledon",
	    "Growver",
	    "Leif",
	    "Chlorophyll",
	    "Xylem",
	    "Phloem"
    });

    static Dictionary<ENT_Body.monsterTypes, randStringList> minionNames = new Dictionary<ENT_DungeonEntity.monsterTypes, randStringList>()
    {
        {
            ENT_Body.monsterTypes.treeMinion,
            new randStringList(new List<string>() {
                "Barky",
	            "Branchy",
	            "Oak",
	            "Eucalyptus",
	            "Sequoia",
	            "Breech",
	            "Elm",
	            "Fir",
	            "Maple",
	            "Sycamore",
	            "Birch",
	            "Dogwood",
	            "Willow",
	            "Stumpy",
	            "Knot",
            })
        },
        {
            ENT_Body.monsterTypes.flowerMinion,
            new randStringList(new List<string>() {
	            "Flowy",
	            "Blossom",
	            "Daisy",
	            "Tulip",
	            "Rose",
	            "Petunia",
	            "Dahlia",
	            "Lily",
	            "Buttercup",
	            "Casia",
	            "Hana",
	            "Heather",
	            "Lotus",
	            "Myrthe",
	            "Pansy"
            })
        },
        {
            ENT_Body.monsterTypes.mushroomMinion,
            new randStringList(new List<string>() {
	            "Shiitake",
	            "Portobello",
	            "Fungus",
	            "Hyphae",
	            "Pileus",
	            "Volva",
	            "Annulus",
	            "Enokitake",
	            "Maitake",
	            "Cremini",
	            "Amanita",
	            "Conocybe",
	            "Cortinarius",
	            "Galerina",
	            "Podostroma"
            })
        },
        {
            ENT_Body.monsterTypes.confusionMinion,
            new randStringList(new List<string>() {
	            "Campanula",
	            "Betulifolia",
	            "Zoysii",
	            "Piperi",
	            "Paryii",
	            "Giffinnii",
	            "Jacobaea",
	            "Latifolia"
            })
        },
        {
            ENT_Body.monsterTypes.necroMinion,
            new randStringList(new List<string>() {
                "Aseroe rubra",
                "Jacques Labillardière",
                "Gastero",
                "Basidia",
                "Anthurus",
                "Colus",
                "Pseudocolus",
                "Neoly",
                "Moritmus"
            })
        },
        {
            ENT_Body.monsterTypes.slowMinion,
            new randStringList(new List<string>() {
	            "Sunny",
	            "Darlingtonia",
	            "Nepenthes",
	            "Bromeliaceae",
	            "Ericales",
	            "Sessile",
	            "Rapun",
	            "Aldrovanda",
	            "Roridula"
            })
        },
    };
    
    static randStringList genModifiers = new randStringList(new List<string>()
    {
	    "the Treeterous",
	    "the Brave",
	    "the Two-Thyme",
	    "the Hungry",
	    "the Tall",
	    "the Luscious",
	    "the Good",
	    "the Bad",
	    "the Ugly",
	    "the Withered",
	    "the Drought-tolerant",
	    "the Dormant",
	    "the Succulent",
	    "the Prickly",
	    "the Wilted",
	    "the Monocot",
	    "the Dicot",
	    "the Blooming",
	    "the Perennial",
	    "the Stalky",
	    "the Grape",
	    "the Berry Best",
	    "the Edible",
	    "the Coniferous"
    });

    /*static Dictionary<ENT_Body.monsterTypes, randStringList> minionModifiers = new Dictionary<ENT_DungeonEntity.monsterTypes, randStringList>()
    {
        {
            ENT_Body.monsterTypes.treeMinion,
            new randStringList(new List<string>() {
                "the Tree1",
                "the Tree2",
                "the Tree3"
            })
        },
        {
            ENT_Body.monsterTypes.flowerMinion,
            new randStringList(new List<string>() {
                "the flower1",
                "the flower2",
                "the flower3"
            })
        },
        {
            ENT_Body.monsterTypes.mushroomMinion,
            new randStringList(new List<string>() {
                "the mushroom1",
                "the mushroom2",
                "the mushroom3"
            })
        },
        {
            ENT_Body.monsterTypes.confusionMinion,
            new randStringList(new List<string>() {
                "the confusion1",
                "the confusion2",
                "the confusion3"
            })
        },
        {
            ENT_Body.monsterTypes.necroMinion,
            new randStringList(new List<string>() {
                "the necro1",
                "the necro2",
                "the necro3"
            })
        },
        {
            ENT_Body.monsterTypes.slowMinion,
            new randStringList(new List<string>() {
                "the slow1",
                "the slow2",
                "the slow3"
            })
        },
    };*/

    static string selectString(randStringList a, randStringList b)
    {
        float val = (float)a.stringsLeft() / (a.stringsLeft() + b.stringsLeft());
        if(Random.value <= val)
        {
            return a.getString();
        }
        else
        {
            return b.getString();
        }
    }

    // Generates a name for a minion
    public static string generateName(ENT_Body.monsterTypes t)
    {
        // Select between general and minion-specific name
        string newName = selectString(genNames, minionNames[t]);

        if (!newName.Contains(" "))
        {
            newName += " " + genModifiers.getString();
        }
        return newName;
    }

    // Incriments the index that we are on in a list
    static void incriment(ref int index, List<string> values)
    {
        ++index;
        if (index == values.Count)
        {
            values.Shuffle();
            index = 0;
        }
    }
}

static class ListUtil
{
    public static void Swap<T>(this IList<T> list, int i, int j)
    {
        var temp = list[i];
        list[i] = list[j];
        list[j] = temp;
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        for (var i = 0; i < list.Count; i++)
            list.Swap(i, Random.Range(i, list.Count-1));
    }
}