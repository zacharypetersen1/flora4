﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIX_AmmoRadialBar : MonoBehaviour {

    static UIX_AmmoRadialBar bar;
    Image image;

	// Use this for initialization
	void Awake () {
        bar = this;
        image = GetComponent<Image>();
	}
	
	
    //
    // Sets value of radial bar, value should be [0,1]
    //
    public static void setBarValue(float value)
    {
        bar.image.fillAmount = value;
    }
}
