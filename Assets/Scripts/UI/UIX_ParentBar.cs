﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIX_ParentBar : MonoBehaviour {
    public float offset;
    Camera cam;
	// Use this for initialization
	void Start () {
        Vector3 Pos = transform.position;
        Pos.y += offset;
        transform.position = Pos;
        cam = Camera.main;

	}
	
	// Update is called once per frame
	void Update () {
        //transform.rotation = Camera.main.transform.rotation;
        transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward, cam.transform.rotation * Vector3.down);
        //transform.position = new Vector3(transform.position.x, transform.position.y + offset, transform.position.z);

    }
}
