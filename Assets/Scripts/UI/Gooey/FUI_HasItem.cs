﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FUI_HasItem : FUI_FloatingUI
{

    //Path to item in resources folder
    //Ignore "never used error" called by minion body if item is held
    public string path_to_item;

    public override void Awake()
    {
        base.Awake();
        target = gameObject;
    }

    // Use this for initialization
    public override void Start()
    {
        fui = (GameObject)MonoBehaviour.Instantiate(Resources.Load("FloatingUI/Instructions"));
        setOffset(fui);
        fui.GetComponent<Renderer>().material = material;
    }

    public void Update()
    {
        base.update();
    }
}
