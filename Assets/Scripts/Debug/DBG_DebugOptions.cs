﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBG_DebugOptions : MonoBehaviour
{
    public enum DebugSaveFile { tree, flower, mushroom, slow, confusion, necro};

    public bool playerInvulnerable;
    public bool alliesInvulnerable;
    public bool allowDebugSpawnAllies;
    public bool skipPauseTutorials;

    public bool createDebugSaves;
    public bool loadDebugSaves;
    public bool destroyAllCrystals;
    public DebugSaveFile debugSaveFile;

    private PLY_AllyManager am;

    private static DBG_DebugOptions singleton;

    public static DBG_DebugOptions Get()
    {
        return singleton;
    }

    public void DebugSpawnAlly(ENT_Body.monsterTypes minionType)
    {
        am.wateredCount++;
        am.wateredCounts[minionType]++;

        Vector3 spawnPosition = GetAllySpawnPosition();
        UTL_Dungeon.spawnMinion(minionType, spawnPosition, true);
    }

    public void DebugSpawnSprout(ENT_Body.monsterTypes minionType)
    {
        ABI_Purify.rezUnlocked = true;
        ENT_Remnant.debugUnlockRez = true;
        ENT_PlayerBody playerBody = am.gameObject.GetComponent<ENT_PlayerBody>();
        if (!playerBody.unlocked_types.Contains(minionType))
        {
            playerBody.unlocked_types.Add(minionType);
        }

        Vector3 spawnPosition = GetAllySpawnPosition();
        UTL_Dungeon.spawnSprout(minionType, spawnPosition, null);
    }

    private Vector3 GetAllySpawnPosition()
    {
        Vector3 playerPos = am.gameObject.transform.position;

        float randomAngle = Random.Range(0.0f, 360.0f);
        Vector3 unitVec = UTL_Math.angleRadToUnitVec(Mathf.Deg2Rad * randomAngle, 0.0f);
        float randomDist = Random.Range(2.0f, 5.0f);
        Vector3 offset = unitVec * randomDist;

        return playerPos + offset;
    }

	// Use this for initialization
	void Start () {
        //don't enable debug options in built executable
#if !UNITY_EDITOR
        playerInvulnerable = false;
        alliesInvulnerable = false;
        allowDebugSpawnAllies = false;
        skipPauseTutorials = false;
        createDebugSaves = false;
        loadDebugSaves = false;
#else
        am = GameObject.FindWithTag("Player").GetComponent<PLY_AllyManager>();
        if (!am)
            Debug.LogError("Could not find AllyManager!");
#endif

        singleton = this;
	}

    void Update()
    {
#if UNITY_EDITOR
        if (allowDebugSpawnAllies)
        {
            //Alt
            if (INP_PlayerInput.getButton("ChangeSelectRadius"))
            {
                if (INP_PlayerInput.getButtonDown("DebugSpawnAlly1"))
                {
                    DebugSpawnSprout(ENT_Body.monsterTypes.treeMinion);
                }
                else if (INP_PlayerInput.getButtonDown("DebugSpawnAlly2"))
                {
                    DebugSpawnSprout(ENT_Body.monsterTypes.flowerMinion);
                }
                else if (INP_PlayerInput.getButtonDown("DebugSpawnAlly3"))
                {
                    DebugSpawnSprout(ENT_Body.monsterTypes.confusionMinion);
                }
                else if (INP_PlayerInput.getButtonDown("DebugSpawnAlly4"))
                {
                    DebugSpawnSprout(ENT_Body.monsterTypes.slowMinion);
                }
                else if (INP_PlayerInput.getButtonDown("DebugSpawnAlly5"))
                {
                    DebugSpawnSprout(ENT_Body.monsterTypes.confusionMinion);
                }
                else if (INP_PlayerInput.getButtonDown("DebugSpawnAlly6"))
                {
                    DebugSpawnSprout(ENT_Body.monsterTypes.necroMinion);
                }
            }
            else
            {
                if (INP_PlayerInput.getButtonDown("DebugSpawnAlly1"))
                {
                    DebugSpawnAlly(ENT_Body.monsterTypes.treeMinion);
                }
                else if (INP_PlayerInput.getButtonDown("DebugSpawnAlly2"))
                {
                    DebugSpawnAlly(ENT_Body.monsterTypes.flowerMinion);
                }
                else if (INP_PlayerInput.getButtonDown("DebugSpawnAlly3"))
                {
                    DebugSpawnAlly(ENT_Body.monsterTypes.confusionMinion);
                }
                else if (INP_PlayerInput.getButtonDown("DebugSpawnAlly4"))
                {
                    DebugSpawnAlly(ENT_Body.monsterTypes.slowMinion);
                }
                else if (INP_PlayerInput.getButtonDown("DebugSpawnAlly5"))
                {
                    DebugSpawnAlly(ENT_Body.monsterTypes.confusionMinion);
                }
                else if (INP_PlayerInput.getButtonDown("DebugSpawnAlly6"))
                {
                    DebugSpawnAlly(ENT_Body.monsterTypes.necroMinion);
                }
            }
            
        }
#endif
    }

#if UNITY_EDITOR
    public string GetDebugSaveFileName()
    {
        //if (debugSaveFileName.Contains(".flora"))
        //    return debugSaveFileName;
        //else if (debugSaveFileName.Length > 0)
        //{
        //    string saveFileName = debugSaveFileName + ".flora";
        //    return saveFileName;
        //}
        //else
        //    return "debugSave.flora";
        switch (debugSaveFile)
        {
            case DebugSaveFile.tree:
                return "tree.flora";
            case DebugSaveFile.flower:
                return "flower.flora";
            case DebugSaveFile.mushroom:
                return "mushroom.flora";
            case DebugSaveFile.slow:
                return "slow.flora";
            case DebugSaveFile.confusion:
                return "confusion.flora";
            case DebugSaveFile.necro:
                return "necro.flora";
            default:
                return "INVALID_SAVE_FILE_SELECTION";
        }
    }
#endif
}
