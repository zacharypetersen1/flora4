﻿//Devon

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
// Camera state when camera is returning to player, player has control
//
public class CAM_Returning : CAM_State {
    private float timeCounter = 0;
    private float slideSpeed = 0.00f;


    //
    // runs once per frame
    // Returns camera back to free position, "f" value controls slide speed
    //
    public override void FixedUpdate()
    //public override void Update()
    {
        dolly.adjust();
        Transform target = dolly.getCamTransform();
        slideSpeed += (float)(3.0 / 3) * Time.fixedDeltaTime;
        cameraScript.transform.position = Vector3.Lerp(cameraScript.transform.position, target.position, slideSpeed);
        cameraScript.transform.rotation = Quaternion.Lerp(cameraScript.transform.rotation, target.rotation, slideSpeed);
        timeCounter += Time.fixedDeltaTime;
        if (timeCounter >= 3)
        {
            CAM_Camera.setState(CAM_State.type.free);
        }
    }
}
