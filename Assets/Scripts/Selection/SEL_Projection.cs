﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEL_Projection : MonoBehaviour {

    //these different materials are created at tools time to prevent the engine
    //instantiating a different version of each material for each minion at runtime
    public Material currentSelectionMat;
    public Material groupSelectionMat;
    public Projector projector;

    public enum selectionMode {current, group };

    public void Reset()
    {
        SetProjectionMode(selectionMode.current);
    }

	public void SetProjectionMode(selectionMode newMode)
    {
        switch (newMode)
        {
            case selectionMode.current:
                SetMaterial(currentSelectionMat);
                break;
            case selectionMode.group:
                SetMaterial(groupSelectionMat);
                break;
        }
    }

    private void SetMaterial(Material mat)
    {
        if (!projector)
            projector = GetComponent<Projector>();

        projector.material = mat;
    }
}
