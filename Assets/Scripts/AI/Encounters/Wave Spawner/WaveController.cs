﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WaveController : MonoBehaviour {

    //MAKE SURE ALL SPAWNERS ARE SET WITH SPHERE COLLIDERS
    // 3 waves, First = all tier 1, Second = tier 1 & confusion/slow, Third = everything
    List<GameObject> spawners;
    [HideInInspector]
    public List<GameObject> spawningPool = new List<GameObject>();

    //WHAT IS CENTRAL FOCUS FOR ENEMY MINIONS TO TRY TO KILL HERE
    Vector3 attackTarget;
    
    float waveTimer = 0;   //HOW LONG IS EACH WAVE
    int waveMinionCount; //HOW MANY MINIONS IN THE WAVE
    int maxWaveMinionCount;
    int maxTierTwoInWave;
    int tierTwoMinionCount = 0;

    public enum Wave { none, One, Two, Three};
    public Wave Current_Wave
    {
        set
        {
            currentWave = value;
            //Debug.Log("Current Wave " + currentWave);
            switch (currentWave)
            {
                case Wave.One:
                    waveTimer = waveOneTimer;
                    waveMinionCount = waveOneMinionCount;
                    maxWaveMinionCount = waveOneMinionCount;
                    maxTierTwoInWave = 0;
                    tierTwoMinionCount = 0;
                    spawnWave(currentWave);
                    break;
                case Wave.Two:
                    waveTimer = waveTwoTimer;
                    waveMinionCount = waveTwoMinionCount;
                    maxWaveMinionCount = waveTwoMinionCount;
                    maxTierTwoInWave = 5;
                    tierTwoMinionCount = 0;
                    spawnWave(currentWave);
                    break;
                case Wave.Three:
                    waveTimer = waveThreeTimer;
                    waveMinionCount = waveThreeMinionCount;
                    maxWaveMinionCount = waveThreeMinionCount;
                    maxTierTwoInWave = 9;
                    tierTwoMinionCount = 0;
                    spawnWave(currentWave);
                    break;
                default:
                    break;
            }
        }
        get { return currentWave; }
    }
    Wave currentWave = Wave.none;
    public float waveOneTimer = 300;      
    public int waveOneMinionCount = 50; 
    public float waveTwoTimer = 300;
    public int waveTwoMinionCount = 50;
    public float waveThreeTimer = 500000; //Not sure what happens after the last wave timer is up
    public int waveThreeMinionCount = 50;

    bool transitionToNextWave = false;

    public bool battleWon = false;

    //CALL THIS FUNCTION TO BEGIN THE WAVE SPAWNING EVENT
    public void startFinalBattle()
    {
        Debug.Log("Started");
        Current_Wave = Wave.One;
    }


    void Start () {
        spawners = new List<GameObject>();
        for(int i = 0; i < gameObject.transform.childCount; ++i)
        {
            spawners.Add(gameObject.transform.GetChild(i).gameObject);
        }

        attackTarget = GameObject.Find("GrandGuardians").transform.position;

    }
	
	// Update is called once per frame
	void Update () {
        //
        if (Current_Wave != Wave.none)
        {
            waveTimer -= TME_Manager.getDeltaTime(TME_Time.type.environment);
            //Debug.Log(waveTimer);
            //cases to start the next wave after one
            //Debug.Log("transitionToNextWave : " + !transitionToNextWave);
            if ((waveTimer <= 0 || spawningPool.Count <= (maxWaveMinionCount * .10)) && !transitionToNextWave)
            {
                //Debug.Log("End of " + currentWave);
                transitionToNextWave = true;
                StartCoroutine(WaitToSpawnNextWave());
            }
           // Debug.Log("Current Wave = " + Current_Wave);
            if(Current_Wave == Wave.Three && spawningPool.Count <= 0)
            {
                Debug.Log("BATTLE WON ");
                battleWon = true;
            }
        }
	}

    void StartNextWave()
    {
        
        switch (Current_Wave)
        {
            case Wave.One:
                Current_Wave = Wave.Two;
                break;
            case Wave.Two:
                Current_Wave = Wave.Three;
                break;
            case Wave.Three:
                break;
            default:
                break;
        }
        //Debug.Log("Start of " + currentWave);
        return;
    }

    IEnumerator cycleSpawnersDeployMinions(List<int> typesToSpawn)
    {
        int numToSpawn = 3;
        switch (currentWave)
        {
            case Wave.One:
                numToSpawn = 3;
                break;
            case Wave.Two:
                numToSpawn = 4;
                break;
            case Wave.Three:
                numToSpawn = 5;
                break;
            default:
                Debug.Log("Trying to spawn minions not in a wave!!");
                break;
        }

        //loop through each spawner
        foreach (var spawn in spawners)
        {
            //set random type and position to spawn in spawner
            for (int i = 0; i < numToSpawn; ++i)
            {
                Vector3 spawnLocation = UTL_Dungeon.getRandomPointInCircle(spawn.transform.position, spawn.GetComponent<SphereCollider>().radius);
                int randomType = 0;
                //spawn tier ones
                if (tierTwoMinionCount > maxTierTwoInWave)
                {
                    randomType = Random.Range(0, 3);
                }
                else
                {
                    //spawn tier twos if max tier two count hasnt been hit
                    randomType = Random.Range(0, typesToSpawn.Count);
                    if (randomType > 2)
                    {
                        ++tierTwoMinionCount;
                    }
                }
                
                
                GameObject minionType = UTL_Dungeon.spawnMinion((ENT_DungeonEntity.monsterTypes)typesToSpawn[randomType], spawnLocation, false, new List<Vector3> { attackTarget }, false, true);
                spawningPool.Add(minionType);
                minionType.GetComponent<ENT_Body>().inFinalWavesBattle = true;

                --waveMinionCount;
            }
        }
        yield return new WaitForSeconds(5);
        if (waveMinionCount > 0) StartCoroutine(cycleSpawnersDeployMinions(typesToSpawn));
 
    }

    void spawnWave(Wave spawningWave)
    {
        //Sets minion to spawn
        switch (spawningWave)
        {
            case Wave.One:
                //List numbers corespond to ENT_DungeonEntity.monsterTypes
                StartCoroutine(cycleSpawnersDeployMinions(new List<int> {0,1,2}));
                break;
            case Wave.Two:
                StartCoroutine(cycleSpawnersDeployMinions(new List<int> {0,1,2,3,5}));
                break;
            case Wave.Three:
                StartCoroutine(cycleSpawnersDeployMinions(new List<int> {0,1,2,3,4,5}));
                break;
            default:
                break;
        }
    }

    IEnumerator WaitToSpawnNextWave()
    {
        //Debug.Log("In spawn next wave");
        yield return new WaitForSeconds(3);
        StartNextWave();
        //Debug.Log("Spawned wave");
        transitionToNextWave = false;
    }

    //IF THE PLAYER LOSES THE FIGHT RESET THE WAVES
    public void resetFight()
    {
		if(currentWave == Wave.One) StopCoroutine (cycleSpawnersDeployMinions(new List<int> {0,1,2}));
		else if (currentWave == Wave.Two) StopCoroutine (cycleSpawnersDeployMinions(new List<int> {0,1,2,3,5}));
		else if (currentWave == Wave.Three) StopCoroutine(cycleSpawnersDeployMinions(new List<int> {0,1,2,3,4,5}));

		foreach (var minion in spawningPool.ToList())
        {
            minion.GetComponent<ENT_Body>().cleanse_self();
			//minion.GetComponent<ENT_Body>().CL = 0;
        }
        currentWave = Wave.none;
		waveTimer = 0;
    }
}
