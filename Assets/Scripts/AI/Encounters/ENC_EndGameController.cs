﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ENC_EndGameController : MonoBehaviour {

    bool startFinalBattle = true;
    WaveController finalBattleController;
	ENT_PlayerBody player;
	ENT_Structure GrandGaurdians;
	public bool isStarted{
		get{
			return finalBattleController.Current_Wave != WaveController.Wave.none;
		}
	}

    // Use this for initialization
    void Start () {
		GrandGaurdians = GameObject.Find("GrandGuardians").GetComponent<ENT_Structure>();
		finalBattleController = transform.gameObject.GetComponentInChildren<WaveController> ();
		player = GameObject.Find("Player").GetComponent<ENT_PlayerBody>();
    }
	
	// Update is called once per frame
	void Update () {
		if (finalBattleController.battleWon) {
			//end game here
			Debug.Log("GAME OVER");
			SceneManager.LoadScene ("VictoryCredits",LoadSceneMode.Single);
		}
			
	}

	//Called in ENT_PlayerBody
	public IEnumerator resetFinalBattle()
	{
		yield return new WaitForSeconds(2);
		startFinalBattle = true;
		GrandGaurdians.CL = GrandGaurdians.CL_MAX;
		finalBattleController.resetFight();

        //show end game messages again
        ++TUT_TutorialPopup.numTimesFinalBattleReset;
	}

    IEnumerator beginFinalBattle()
    {
        yield return new WaitForSeconds(2);
        finalBattleController.startFinalBattle();
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag == "Player" && startFinalBattle)
        {
            StartCoroutine(beginFinalBattle());
            startFinalBattle = false;
        }
    }

    public void ScriptStartFinalBattle()
    {
        if (startFinalBattle)
        {
            StartCoroutine(beginFinalBattle());
            startFinalBattle = false;
        }
        else
        {
            StartCoroutine(resetFinalBattle());
        }
    }

}
