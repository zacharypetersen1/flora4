﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TME_MasterTime : TME_Time {



    //
    // constructor
    //
    public TME_MasterTime()
    {
        tag = type.master;
    }



    //
    // Returns the delta time based on the time scalar for this time
    //
    public override float getDeltaTime()
    {
        return Time.deltaTime * scalar;
    }



    //
    // Returns the time scalar that is currently being applied to the delta time
    //
    public override float getTimeScalar()
    {
        return scalar;
    }
}
